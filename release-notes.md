# Release Notes

## Version 1.3.0

Added additional US regions and localities to `com.treangenlab.harvest_variants.ncbi.sra.geonames`.