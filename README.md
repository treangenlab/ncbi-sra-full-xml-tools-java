# NCBI SRA Full Xml Tools for Java

## Description

Provides an XML parser that exposes a subset of the information within an NCBI SRA Full XML metadata file. The NCBI 
Sequence Read Archive search page, located at [https://www.ncbi.nlm.nih.gov/sra](https://www.ncbi.nlm.nih.gov/sra), offers the 
ability to download search results as a single "Full XML" output file such as [this](https://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?save=efetch&rettype=exp&db=sra&WebEnv=MCID_6296920f2d5d1d59cf18abad&query_key=1). 
This library parses such output files to allow programmatic access to contained values such as run accession ids, 
sample accession ids, and organism names--in addition to other values.

This library is designed to parse multi-gigabyte search result files while not exhausting the memory resources of a JVM 
process launched with default parameters on a typical laptop. For example, it has been successfully tested on a 12 GB 
input while consuming less than 7.6 GB of heap.

## Motivating Problem

The use of existing off the shelf Java XML parsers to parse multi-GB result files was found to consume an untenable 
amount of memory resulting in early process termination.

## Installation

This library can be installed into the local Maven repository of the host machine using the Maven command

```
mvn install
```
executed from the same directory as `pom.xml`

## Building

This project uses Maven as its build system.  To compile the library into jar form, execute the Maven command:

```
mvn package
```

After execution, two jar files will be produced in the `target/` directory: `ncb-sra-full-xml-tools-[VER].jar` and 
`ncb-sra-full-xml-tools-[VER]-jar-with-dependencies.jar`.  The first contains only the compiled source of this project.
The second contains the compiled source of this project and all third party dependencies.

## Example Usage

```java

import com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser.ExperimentPackageSetNode;
import com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser.ExperimentPackageSetXmlParser;
import com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser.ParseException;
...

ExperimentPackageSetNode packageSet;
try(FileInputStream inputStream = new FileInputStream(sraXmlFile))
{
    packageSet = ExperimentPackageSetXmlParser.make().parse(inputStream);
}
catch(ParseException e)
{
    throw new RuntimeException(e);
}
```

## Support

This work is supported by the Centers for Disease Control and Prevention (CDC) contract 75D30121C11180.

## License

MIT License

Copyright (c) 2022 Treangen Lab.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

