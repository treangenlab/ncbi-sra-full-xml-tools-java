package com.treangenlab.harvest_variants.ncbi.sra.geonames;

import java.util.Optional;

/**
 * An object that attempts to parse human friendly geographic tags (of a loosely defined or unspecified format) and
 * extract any country / region / locality labels from the tags.
 */
public interface GeoExtractor
{
    /**
     * The found name parts that match a country / region / locality as identified by {@link #extract(String)}.
     */
    interface Result
    {
        /**
         * @return the country with a name that matches a found name part, if any. never {@code null}.
         */
        Optional<Country> getCountry();

        /**
         * @return the region with a name that matches a found name part, if any. never {@code null}.
         */
        Optional<Region> getRegion();

        /**
         * @return the locality with a name that matches a found name part, if any. never {@code null}.
         */
        Optional<Locality> getLocality();

        /**
         * A result with no assigned labels.
         */
        Result Empty = new Result()
        {
            @Override
            public Optional<Country> getCountry()
            {
                return Optional.empty();
            }

            @Override
            public Optional<Region> getRegion()
            {
                return Optional.empty();
            }

            @Override
            public Optional<Locality> getLocality()
            {
                return Optional.empty();
            }
        };

        /**
         * Makes a new result.
         *
         * @param country the country with a name that matches a found name part or {@code null} if no match was found.
         * @param region the region with a name that matches a found name part or {@code null} if no match was found.
         * @param locality the locality with a name that matches a found name part or {@code null} if no match was
         *                 found.
         * @return the new result. never {@code null}.
         */
        static Result make(Country country, Region region, Locality locality)
        {
            return new Result()
            {
                @Override
                public Optional<Country> getCountry()
                {
                    return country == null ? Optional.empty() : Optional.of(country);
                }

                @Override
                public Optional<Region> getRegion()
                {
                    return region == null ? Optional.empty() : Optional.of(region);
                }

                @Override
                public Optional<Locality> getLocality()
                {
                    return locality == null ? Optional.empty() : Optional.of(locality);
                }
            };
        }

    }

    /**
     * Extracts locality information from the given string in an implementation specific way.
     *
     * @param geoString the source of geographic information.
     * @return the location information contained in {@code geoString}
     * @throws GeoExtractionException if there is an implementation specific error in extracting locality information.
     */
    Result extract(String geoString) throws GeoExtractionException;
}


