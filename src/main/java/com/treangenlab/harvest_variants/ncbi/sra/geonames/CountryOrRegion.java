package com.treangenlab.harvest_variants.ncbi.sra.geonames;

/**
 * Either a {@link Country} or {@link Region} instance.
 */
public abstract class CountryOrRegion
{
    CountryOrRegion() // package private to help assert satisfaction of class contract instance constraints
    {
    }

    // visitor pattern
    interface Cases<R>
    {
        R regionCase(Region region);

        R countryCase(Country country);
    }

    // visitor pattern
    public abstract <R> R which(Cases<R> cases);

    /**
     * @return the name of the country or region. never {@code null}.
     */
    public abstract String getName();

    /**
     * @return the country component of this {@code Country} or {@code Region}. never {@code null}.
     */
    public abstract Country getCountry();

    /**
     * Tests if this object and the given object are the same logical country or region.
     *
     * @param obj the candidate object. may be {@code null}.
     * @return {@code true} if the given object is an instance of either {@link Country} or {@link Region} and is the
     *         same (logical) country or region as this object. Otherwise {@code false}.
     */
    @Override
    public abstract boolean equals(Object obj);

}
