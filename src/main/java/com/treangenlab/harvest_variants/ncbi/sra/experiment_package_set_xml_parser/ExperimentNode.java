package com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser;

import java.util.List;

/**
 * Models the {@code EXPERIMENT} section of an NCBI SRA Full Xml record.
 */
public interface ExperimentNode
{
    /**
     * @return any {@code PLATFORM} child elements of this experiment node. never @{code null}.
     */
    List<PlatformNode> getPlatforms();
}
