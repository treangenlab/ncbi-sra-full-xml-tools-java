package com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * A partial implementation of {@code ExperimentPackageSetXmlParser} that is able to parse multi-gigabyte xml inputs
 * consisting of many smaller {@code EXPERIMENT_PACKAGE} elementas. Subclasses need only provide a method for parsing
 * one {@code EXPERIMENT_PACKAGE} at a time in string form.
 */
public abstract class ExperimentPackageSetXmlParserTemplate implements ExperimentPackageSetXmlParser
{
    /**
     * Logger for class.
     */
    private final Logger _logger = LoggerFactory.getLogger(ExperimentPackageSetXmlParserTemplate.class);

    // contract from super
    @Override
    public ExperimentPackageSetNode parse(InputStream xmlStream) throws IOException, ParseException
    {
        _logger.debug("entering");

        if(xmlStream == null)
        {
            _logger.warn("xmlStream null");
            _logger.debug("exiting");
            throw new NullPointerException("xmlStream");
        }

        // xmlInput is expected to be on the scale of 1-2 gigs, so create a large buffer to speed reads
        int bufferSizeInBytes = (int)Math.pow(2, 28); // 2^28 bytes ~~ 268 megabytes
        ExperimentPackageSetNode node =
                parseHelp(new PushbackInputStreamSize2(new BufferedInputStream(xmlStream, bufferSizeInBytes)));

        _logger.debug("exiting");
        return node;
    }

    /**
     * Parses the given xml stream. Expected to be of the general structure
     * {@code "<?xml version="1.0"?>&lt;EXPERIMENT_PACKAGE_SET&gt;&lt;EXPERIMENT_PACKAGE&gt;...&lt;/EXPERIMENT_PACKAGE>&lt;/EXPERIMENT_PACKAGE_SET&gt;" }
     *
     * @param xmlStream the UTF-8 encoded xml stream. may not be {@code null}.
     * @return a representation of the root experiment package set. never {@code null}.
     * @throws ParseException if the format of the stream's bytes is not valid XML or not does conform to the expected
     *                        structure
     */
    private ExperimentPackageSetNode parseHelp(PushbackInputStreamSize2 xmlStream) throws IOException, ParseException
    {
        _logger.debug("entering");

        if(xmlStream == null)
        {
            _logger.warn("xmlStream null");
            _logger.debug("exiting");
            throw new NullPointerException("xmlStream");
        }

        /*
         * Read past initial <?xml version="1.0"?> of document.
         */
        int firstChar  = xmlStream.read();
        int secondChar = xmlStream.read();
        if(firstChar != 60 || secondChar != 63)
        {
            _logger.warn("xmlStream did not start with <?");
            _logger.debug("exiting");
            throw new ParseException("Expected xml to start with <?");
        }

        int nextChar = xmlStream.read();
        while(nextChar != -1 && nextChar != 62) // skip to just past the > of <?xml version="1.0"?>
            nextChar = xmlStream.read();

        /*
         * Read past initial \n<EXPERIMENT_PACKAGE_SET> of document
         */
        String expectedPackageSetChars = "\n<EXPERIMENT_PACKAGE_SET>";
        byte[] firstChars = xmlStream.readNBytes(expectedPackageSetChars.length());
        if (!expectedPackageSetChars.equals(new String(firstChars)))
        {
            _logger.warn("expected start of experiment package set");
            _logger.debug("exiting");
            throw new ParseException("Expected " + expectedPackageSetChars);
        }

        discardLeadingWhitespace(xmlStream);

        List<ExperimentPackageNode> packages = new LinkedList<>();
        while (doesStreamAppearToBeStartingNewExperimentPackage(xmlStream))
        {
            packages.add(parsePackage(xmlStream));
            discardLeadingWhitespace(xmlStream);
        }

        String expectedFinalChars = "</EXPERIMENT_PACKAGE_SET>";
        byte[] finalChars = xmlStream.readNBytes(expectedFinalChars.length());
        if (!expectedFinalChars.equals(new String(finalChars)))
        {
            _logger.warn("expected end of experiment package set");
            _logger.debug("exiting");
            throw new ParseException("Expected stream to end with " + expectedFinalChars + " not " +
                                      new String(finalChars));
        }

        /*
         * Check that the only characters after the final xml element are whitespace.
         */
        discardLeadingWhitespace(xmlStream);

        if(xmlStream.read() != -1)
        {
            _logger.warn("Unexpected trailing character in document");
            _logger.debug("exiting");
            throw new ParseException("Unexpected trailing character in document");
        }

        _logger.debug("exiting");
        //noinspection Convert2Lambda - lambda here reduces readability
        return new ExperimentPackageSetNode()
        {
            @Override
            public List<ExperimentPackageNode> getPackages()
            {
                return Collections.unmodifiableList(packages);
            }
        };
    }

    // read and discard all whitespace characters at the front of the stream
    private void discardLeadingWhitespace(PushbackInputStreamSize2 peekableStream) throws IOException
    {
        _logger.debug("entering");

        while(true)
        {
            int nextChar = peekableStream.read();
            if(nextChar == -1)
                break;

            if(Character.isWhitespace(nextChar))
                continue;

            peekableStream.unread(nextChar);
            break;
        }

        _logger.debug("exiting");
    }

    // tests if the given stream has at least two more characters and those characters are '<' followed by 'E'
    static boolean doesStreamAppearToBeStartingNewExperimentPackage(PushbackInputStreamSize2 peekableStream)
            throws IOException
    {
        Logger logger = LoggerFactory.getLogger(ExperimentPackageSetXmlParserTemplate.class);
        logger.debug("entering");

        int nextChar = peekableStream.read();
        int nextNextChar = peekableStream.read();
        peekableStream.unread(nextNextChar);
        peekableStream.unread(nextChar);

        if (nextChar == -1 || nextNextChar == -1)
        {
            logger.debug("exiting");
            return false;
        }

        // need to look further than one char because expecting <EXPERIMENT_PACKAGE> or </EXPERIMENT_PACKAGE>
        boolean result = nextChar == '<' && nextNextChar == 'E';

        logger.debug("exiting");
        return result;
    }

    // parse the given "<EXPERIMENT_PACKAGE>" to "</EXPERIMENT_PACKAGE>" from the front of the stream
    private ExperimentPackageNode parsePackage(InputStream xmlInput) throws IOException, ParseException
    {
        _logger.debug("entering");

        /*
         * Assert first stream chars are the expected opening tag.
         */
        String expectedFirstChars = "<EXPERIMENT_PACKAGE>";
        byte[] firstChars = xmlInput.readNBytes(expectedFirstChars.length());
        if(!expectedFirstChars.equals(new String(firstChars)))
        {
            _logger.warn("not start of experiment package");
            _logger.debug("exiting");
            throw new ParseException("Expected stream to start with " + expectedFirstChars + " but found " +
                                      new String(firstChars));
        }

        /*
         * Collect all chars from "<EXPERIMENT_PACKAGE>" to "</EXPERIMENT_PACKAGE>" in experimentPackageXmlBuilder.
         */
        StringBuilder experimentPackageXmlBuilder = new StringBuilder(expectedFirstChars);
        int prevPrevChar = -1;
        int prevChar = -1;
        int nextChar = xmlInput.read();
        while(true)
        {
            if(nextChar == -1)
            {
                _logger.warn("xmlInput exhausted");
                _logger.debug("exiting");
                throw new ParseException("Unexpected end of stream");
            }

            experimentPackageXmlBuilder.append((char)nextChar);

            if(prevPrevChar == 'G' &&  prevChar == 'E' && nextChar == '>' &&
              experimentPackageXmlBuilder.toString().endsWith("</EXPERIMENT_PACKAGE>"))
                break;

            prevPrevChar = prevChar;
            prevChar = nextChar;
            nextChar = xmlInput.read();

        }

        /*
         * Parse the collected chars.
         */
        ExperimentPackageNode parsedPackageNode = parseExperimentPackageString(experimentPackageXmlBuilder.toString());
        _logger.debug("exiting");
        return parsedPackageNode;
    }

    /**
     * Parses an experiment package xml element in string form. The input string is expected to be of the general form
     * {@code &lt;EXPERIMENT_PACKAGE&gt;...&lt;/EXPERIMENT_PACKAGE&gt;}.
     *
     * @param experimentPackageXml the experiment package. may not be {@code null}.
     * @return the parsed package element. never {@code null}.
     * @throws ParseException if the given string does not conform to the expected format
     */
    protected abstract ExperimentPackageNode parseExperimentPackageString(String experimentPackageXml)
            throws ParseException;
}
