package com.treangenlab.harvest_variants.ncbi.sra.geonames;

import java.util.Arrays;
import java.util.List;

/**
 * A named country of the world.
 */
public class Country extends CountryOrRegion
{
    /**
     * The set of valid country names.
     */
    private static final List<String> _knownCountries =
            Arrays.asList("Angola", "Argentina", "Australia", "Austria", "Bangladesh", "Bhutan", "Brazil", "Canada",
                          "Croatia", "China", "Czech Republic", "Dominican Republic", "Estonia", "Ethiopia", "Finland",
                          "France", "Germany", "Greece", "Indonesia", "India", "Iran", "Iraq", "Ireland", "Israel",
                          "Italy", "Japan", "Jordan", "Latvia", "Malawi", "Malaysia", "Mauritius", "Mexico",
                          "Mozambique", "Nepal", "Netherlands", "Norway", "Pakistan", "Peru", "Philippines", "Poland",
                          "Portugal", "Qatar", "Romania", "Russia", "Saudi Arabia", "Slovakia", "South Africa",
                          "State of Palestine", "Serbia", "Singapore", "Spain", "Sweden", "Switzerland", "Thailand",
                          "Uganda", "United Kingdom", "Uruguay", "USA", "Zimbabwe");

    /**
     * The name of the country.
     */
    private final String _name;

    /**
     * Creates a new country with the given name.
     *
     * @param name the name of the country. may be {@code null}.
     * @throws IllegalArgumentException if the given name is not known to be the name of a country.
     */
    private Country(String name)
    {
        if(!_knownCountries.contains(name))
            throw new IllegalArgumentException("Unknown country: " + name);

        _name = name;
    }

    /**
     * Finds the country with the given name (case-sensitive).
     *
     * @param name the name of the country. may not be {@code null}.
     * @return the country with the given name. never {@code null}.
     * @throws IllegalArgumentException if no country with the given name is found.
     */
    public static Country get(String name)
    {
        if(name == null)
            throw new NullPointerException("name");

        return new Country(name);
    }

    // contract from super
    @Override
    public boolean equals(Object other)
    {
        return other instanceof Country && this.equals((Country) other);
    }

    /**
     * Tests if this country and the given country are the same country.
     *
     * @param other the candidate country. may be {@code null}.
     * @return {@code true} if the given country is non-null and has the same name as this country as tested by
     *         {@link String#equals(Object)}. Otherwise {@code false}.
     */
    public boolean equals(Country other)
    {
        if(other == null)
            return false;

        return other._name.equals(this._name);
    }

    // visitor pattern
    @Override
    public <R> R which(Cases<R> cases)
    {
        return cases.countryCase(this);
    }

    // contract from super
    @Override
    public int hashCode()
    {
        return _name.hashCode();
    }

    // contract from super
    @Override
    public String getName()
    {
        return _name;
    }

    // contract from super
    @Override
    public Country getCountry()
    {
        return this;
    }
}
