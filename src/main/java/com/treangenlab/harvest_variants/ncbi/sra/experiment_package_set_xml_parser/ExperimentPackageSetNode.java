package com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser;
import java.util.List;

/**
 * Models the {@code EXPERIMENT_PACKAGE_SET} section of an NCBI SRA Full Xml record.
 */
public interface ExperimentPackageSetNode
{
    /**
     * @return any {@code <EXPERIMENT_PACKAGE>} child elements of this experiment package set node. never @{code null}.
     */
    List<ExperimentPackageNode> getPackages();
}
