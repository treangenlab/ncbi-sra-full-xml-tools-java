package com.treangenlab.harvest_variants.ncbi.sra.sequenced_sample;

import com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser.*;

import java.util.*;

class SequencedSampleSetBuilderDefault implements SequencedSampleSetBuilder
{
    private boolean _built = false;

    private int _skippedSamplesCount = 0;

    private Set<String> _seenSampleIds = new HashSet<>();

    private List<SequencedSample> _sequencedSamplesAccum = new LinkedList<>();

    @Override
    public SequencedSampleSet build()
    {
        List<SequencedSample> sequencedSamples = Collections.unmodifiableList(_sequencedSamplesAccum);
        _built = true;
        return new SequencedSampleSet()
        {
            @Override
            public int getSize()
            {
                return sequencedSamples.size();
            }

            @Override
            public Iterator<SequencedSample> iterator()
            {
                return sequencedSamples.iterator();
            }
        };
    }

    @Override
    public int addSamplesFromPackagesSkippingInvalid(ExperimentPackageSetNode packageSet)
    {
        if(_built)
            throw new IllegalStateException();

        int numAdded = 0;
        for (ExperimentPackageNode packageNode : packageSet.getPackages())
        {
            try
            {
                addSampleFromPackage(packageNode);
                numAdded++;
            }
            catch (IllegalArgumentException e)
            {
                _skippedSamplesCount++;
            }
        }

        return numAdded;
    }

    @Override
    public void addSampleFromPackage(ExperimentPackageNode packageNode)
    {
        SampleNode sample;
        {
            List<SampleNode> samples = packageNode.getSamples();

            if(samples.size() != 1)
                throw new IllegalArgumentException();

            sample = samples.get(0);
        }

        String sampleId;
        {
            if (sample.getAccessionId().isEmpty())
                throw new IllegalArgumentException();

            sampleId = sample.getAccessionId().get();
        }

        RunNode run;
        {
            List<RunSetNode> runSets = packageNode.getRunSets();
            if(runSets.size() != 1)
                throw new IllegalArgumentException();


            List<RunNode> runs = runSets.get(0).getRuns();
            if(runs.size() != 1)
                throw new IllegalArgumentException();

            run = runs.get(0);
        }

        ExperimentNode experiment;
        {
            List<ExperimentNode> experiments = packageNode.getExperiments();
            if(experiments.size() != 1)
                throw new IllegalArgumentException();

            experiment = experiments.get(0);
        }

        MemberNode member;
        {
            List<PoolNode> pools = packageNode.getPools();
            if(pools.size() != 1)
                throw new IllegalArgumentException();

            PoolNode pool = pools.get(0);

            List<MemberNode> members = pool.getMembers();
            if(members.size() != 1)
                throw new IllegalArgumentException();

            member = members.get(0);
        }

        SequencingPlatform platform;
        {
            List<PlatformNode> platforms = experiment.getPlatforms();
            if (platforms.size() != 1)
                throw new IllegalArgumentException();

            PlatformNode platformNode = platforms.get(0);

            if (platformNode.getChildElementNames().size() != 1)
                throw new IllegalArgumentException();

            String platformName = platformNode.getChildElementNames().get(0);
            platform = platformName.equalsIgnoreCase("Illumina") ?
                                        SequencingPlatform.Illumina : SequencingPlatform.Other;
        }

        if (_seenSampleIds.contains(sampleId))
            throw new IllegalArgumentException();
        _seenSampleIds.add(sampleId);

        Map<String, List<String>> sampleAttributes = sample.getAttributes();
        List<String> samplePrimaryGeoTags = new LinkedList<>();
        samplePrimaryGeoTags.addAll(sampleAttributes.getOrDefault("geographic location (country and/or sea)",
                                                                  Collections.emptyList()));
        samplePrimaryGeoTags.addAll(sampleAttributes.getOrDefault("geo_loc_name", Collections.emptyList()));
        samplePrimaryGeoTags.addAll(sampleAttributes.getOrDefault("geo loc name", Collections.emptyList()));
        if(samplePrimaryGeoTags.size() > 1)
            throw new IllegalArgumentException();

        List<String> sampleSecondaryGeoTags =
                new LinkedList<>(sampleAttributes.getOrDefault("geographic location (region and locality)",
                                                               Collections.emptyList()));
        if(sampleSecondaryGeoTags.size() > 1)
            throw new IllegalArgumentException();

        List<String> collectionDates = new LinkedList<>();
        collectionDates.addAll(sampleAttributes.getOrDefault("collection date", Collections.emptyList()));
        collectionDates.addAll(sampleAttributes.getOrDefault("collection_date", Collections.emptyList()));
        if(collectionDates.size() > 1)
            throw new IllegalArgumentException();


        Optional<String> samplePrimaryGeoTag =
                samplePrimaryGeoTags.isEmpty() ? Optional.empty() : Optional.of(samplePrimaryGeoTags.get(0));

        Optional<String> sampleSecondaryGeoTag =
                sampleSecondaryGeoTags.isEmpty() ? Optional.empty() : Optional.of(sampleSecondaryGeoTags.get(0));

        Optional<String> sampleCollectionDate =
                collectionDates.isEmpty() ? Optional.empty() : Optional.of(collectionDates.get(0));

        SequencedSample sequencedSample = makeSequencedSample(sampleId, samplePrimaryGeoTag, sampleSecondaryGeoTag,
                                                              sampleCollectionDate, member.getOrganism(), run,
                                                              platform);
        _sequencedSamplesAccum.add(sequencedSample);
    }

    private SequencedSample makeSequencedSample(String sampleId, Optional<String> samplePrimaryGeoTag,
                                                Optional<String> sampleSecondaryGeoTag,
                                                Optional<String> sampleCollectionDate, Optional<String> organism,
                                                RunNode run, SequencingPlatform platform)
    {
        String runId = run.getAccessionId().orElseThrow(IllegalArgumentException::new);
        Optional<String> totalBases = run.getTotalBases();

        return new SequencedSample()
        {
            @Override
            public String getSampleId()
            {
                return sampleId;
            }

            @Override
            public String getRunId()
            {
                return runId;
            }

            @Override
            public Optional<String> getTotalBases()
            {
                return totalBases;
            }

            @Override
            public Optional<String> getPrimaryGeoTag()
            {
                return samplePrimaryGeoTag;
            }

            @Override
            public Optional<String> getSecondaryGeoTag()
            {
                return sampleSecondaryGeoTag;
            }

            @Override
            public Optional<String> getCollectionDate()
            {
                return sampleCollectionDate;
            }

            @Override
            public Optional<String> getOrganism()
            {
                return organism;
            }

            @Override
            public SequencingPlatform getPlatform()
            {
                return platform;
            }
        };
    }

    @Override
    public int getSkippedSamplesCount()
    {
        return _skippedSamplesCount;
    }
}
