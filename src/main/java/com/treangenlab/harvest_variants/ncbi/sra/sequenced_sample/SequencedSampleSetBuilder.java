package com.treangenlab.harvest_variants.ncbi.sra.sequenced_sample;


import com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser.ExperimentPackageNode;
import com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser.ExperimentPackageSetNode;

public interface SequencedSampleSetBuilder
{
    SequencedSampleSet build();

    int getSkippedSamplesCount();

    int addSamplesFromPackagesSkippingInvalid(ExperimentPackageSetNode packageSet);

    void addSampleFromPackage(ExperimentPackageNode packageNode);

    static SequencedSampleSetBuilder make()
    {
        return new SequencedSampleSetBuilderDefault();
    }
}
