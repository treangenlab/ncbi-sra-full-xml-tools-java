package com.treangenlab.harvest_variants.ncbi.sra.sequenced_sample;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public interface SequencedSampleSet extends Iterable<SequencedSample>
{
    int getSize();

    static SequencedSampleSet make(Iterable<SequencedSample> samples)
    {
        Map<String, SequencedSample> idToSample;
        {
            Map<String, SequencedSample> idToSampleAccum = new HashMap<>();
            for (SequencedSample sample : samples)
            {
                String sampleId = sample.getSampleId();
                if (idToSampleAccum.containsKey(sampleId))
                    throw new IllegalArgumentException("duplicate id " + sampleId);

                idToSampleAccum.put(sampleId, sample);
            }
            idToSample = Collections.unmodifiableMap(idToSampleAccum);
        }

        return new SequencedSampleSet()
        {
            @Override
            public int getSize()
            {
                return idToSample.size();
            }

            @Override
            public Iterator<SequencedSample> iterator()
            {
                return idToSample.values().iterator();
            }
        };
    }
}
