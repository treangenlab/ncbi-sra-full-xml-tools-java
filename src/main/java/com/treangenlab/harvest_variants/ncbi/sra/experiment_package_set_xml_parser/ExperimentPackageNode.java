package com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser;

import java.util.List;

/**
 * Models the {@code EXPERIMENT_PACKAGE} section of an NCBI SRA Full Xml record.
 */
public interface ExperimentPackageNode
{
    /**
     * @return any {@code <SAMPLE>} child elements of this experiment package node. never @{code null}.
     */
    List<SampleNode> getSamples();

    /**
     * @return any {@code <RUN_SET>} child elements of this experiment package node. never @{code null}.
     */
    List<RunSetNode> getRunSets();

    /**
     * @return any {@code <EXPERIMENT>} child elements of this experiment package node. never @{code null}.
     */
    List<ExperimentNode> getExperiments();

    /**
     * @return any {@code <POOL>} child elements of this experiment package node. never @{code null}.
     */
    List<PoolNode> getPools();
}
