package com.treangenlab.harvest_variants.ncbi.sra.sequenced_sample;

import com.treangenlab.harvest_variants.ncbi.sra.geonames.SraGeoTagSplitter;

import java.util.Optional;

/**
 * Models the metadata of a DNA sequencing record.
 */
public interface SequencedSample
{
    String getSampleId();

    String getRunId();

    Optional<String> getTotalBases();

    Optional<String> getPrimaryGeoTag();

    Optional<String> getSecondaryGeoTag();

    Optional<String> getCollectionDate();

    Optional<String> getOrganism();

    SequencingPlatform getPlatform();

    default int getGeoTagsNameCount()
    {
        if(getPrimaryGeoTag().isEmpty() && getSecondaryGeoTag().isEmpty())
            return 0;

        String tag = "";
        if(getPrimaryGeoTag().isEmpty())
            tag = getSecondaryGeoTag().get();
        else if(getSecondaryGeoTag().isEmpty())
            tag = getPrimaryGeoTag().get();
        else
            tag = getPrimaryGeoTag().get() + ":" + getSecondaryGeoTag().get();

        return new SraGeoTagSplitter().split(tag).size();

    }
}
