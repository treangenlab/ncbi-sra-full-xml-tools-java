package com.treangenlab.harvest_variants.ncbi.sra.geonames;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A {@code GeoTagSplitter} tailored to the naming conventions of geography tags in sra xml.
 */
public class SraGeoTagSplitter implements GeoTagSplitter
{
    // contract from super
    public List<String> split(String string)
    {
        return  Arrays.stream(string.split("[,/:]"))
                      .map(String::trim)
                      .filter((String part) -> !part.equalsIgnoreCase("not applicable"))
                      .filter((String part) -> !part.equals("NA"))
                      .filter((String part) -> !part.equals("missing"))
                      .filter((String part) -> !part.equals("not collected"))
                      .filter((String part) -> !part.equals(""))
                      .collect(Collectors.toUnmodifiableList());
    }
}
