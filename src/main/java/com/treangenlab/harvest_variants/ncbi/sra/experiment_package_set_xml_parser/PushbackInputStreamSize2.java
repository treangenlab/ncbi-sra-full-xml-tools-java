package com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser;

import java.io.InputStream;
import java.io.PushbackInputStream;

/**
 * A {@code PushbackInputStream} with a size of 2.
 */
class PushbackInputStreamSize2 extends PushbackInputStream
{
    /**
     * Creates a {@code PushbackInputStream}
     * with a pushback buffer of 2,
     * and saves its argument, the input stream
     * {@code in}, for later use. Initially,
     * the pushback buffer is empty.
     *
     * @param  in    the input stream from which bytes will be read.
     * @throws IllegalArgumentException if {@code size <= 0}
     */
    public PushbackInputStreamSize2(InputStream in)
    {
        super(in, 2);
    }
}
