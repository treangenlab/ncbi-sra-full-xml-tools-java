package com.treangenlab.harvest_variants.ncbi.sra.geonames;

import java.util.List;

public interface GeoTagSplitter
{
    /**
     * Splits the given string into geographic name parts filtering out any delimiters or implementation specific
     * placeholder names (e.g. "NA", "none", etc).
     *
     * @param geoString the input string. may not be {@code null}.
     * @return the name parts of the string in the order found in the input string. never {@code null}.
     */
    List<String> split(String geoString);
}
