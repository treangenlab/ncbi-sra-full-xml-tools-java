package com.treangenlab.harvest_variants.ncbi.sra.geonames;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A named region of the world.
 */
public class Region extends CountryOrRegion
{
    private static class NameAndCountry
    {
        private final String Name;
        private final Country Country;

        private NameAndCountry(String name, String country)
        {
            Name = name;
            Country =  com.treangenlab.harvest_variants.ncbi.sra.geonames.Country.get(country);
        }
    }

    /**
     * The set of regions.
     */
    // ordered by Country alpha
    private static final List<NameAndCountry> _knownRegions = Arrays.asList(
            new NameAndCountry("Northern Territory", "Australia"),
            new NameAndCountry("Victoria", "Australia"),

            new NameAndCountry("Chukha", "Bhutan"),
            new NameAndCountry("Sarpang", "Bhutan"),

            new NameAndCountry("Rio de Janeiro", "Brazil"),
            new NameAndCountry("Rio Grande do Sul", "Brazil"),
            new NameAndCountry("Sao Paulo", "Brazil"),

            new NameAndCountry("Ontario", "Canada"),

            new NameAndCountry("Hubei", "China"),

            new NameAndCountry("Alborz", "Iran"),


            new NameAndCountry("Lombardia", "Italy"),

            new NameAndCountry("Telangana", "India"),

            new NameAndCountry("BajaCalifornia", "Mexico"),

            new NameAndCountry("South Holland", "Netherlands"),

            new NameAndCountry("Agder", "Norway"),
            new NameAndCountry("Innlandet", "Norway"),
            new NameAndCountry("More og Romsdal", "Norway"),
            new NameAndCountry("Værøy", "Norway"),
            new NameAndCountry("Rogaland", "Norway"),
            new NameAndCountry("Troms og Finnmark", "Norway"),
            new NameAndCountry("Vestland", "Norway"),
            new NameAndCountry("Viken", "Norway"),

            new NameAndCountry("Amazonas", "Peru"),

            new NameAndCountry("Eastern Region", "Saudi Arabia"),

            new NameAndCountry("Ramallah", "State of Palestine"),

            new NameAndCountry("KZN", "South Africa"),

            new NameAndCountry("Alaska", "USA"),
            new NameAndCountry("Alabama", "USA"),
            new NameAndCountry("Arkansas", "USA"),
            new NameAndCountry("American Samoa", "USA"),
            new NameAndCountry("Arizona", "USA"),
            new NameAndCountry("California", "USA"),
            new NameAndCountry("Colorado", "USA"),
            new NameAndCountry("Connecticut", "USA"),
            new NameAndCountry("District of Columbia", "USA"),
            new NameAndCountry("Delaware", "USA"),
            new NameAndCountry("Florida", "USA"),
            new NameAndCountry("Georgia", "USA"),
            new NameAndCountry("Guam", "USA"),
            new NameAndCountry("Hawaii", "USA"),
            new NameAndCountry("Iowa", "USA"),
            new NameAndCountry("Idaho", "USA"),
            new NameAndCountry("Illinois", "USA"),
            new NameAndCountry("Indiana", "USA"),
            new NameAndCountry("Kansas", "USA"),
            new NameAndCountry("Kentucky", "USA"),
            new NameAndCountry("Louisiana", "USA"),
            new NameAndCountry("Massachusetts", "USA"),
            new NameAndCountry("Maryland", "USA"),
            new NameAndCountry("Maine", "USA"),
            new NameAndCountry("Michigan", "USA"),
            new NameAndCountry("Minnesota", "USA"),
            new NameAndCountry("Missouri", "USA"),
            new NameAndCountry("Mississippi", "USA"),
            new NameAndCountry("Montana", "USA"),
            new NameAndCountry("North Carolina", "USA"),
            new NameAndCountry("North Dakota", "USA"),
            new NameAndCountry("Nebraska", "USA"),
            new NameAndCountry("New Hampshire", "USA"),
            new NameAndCountry("New Jersey", "USA"),
            new NameAndCountry("New Mexico", "USA"),
            new NameAndCountry("Nevada", "USA"),
            new NameAndCountry("New York", "USA"),
            new NameAndCountry("Ohio", "USA"),
            new NameAndCountry("Oklahoma", "USA"),
            new NameAndCountry("Oregon", "USA"),
            new NameAndCountry("Pennsylvania", "USA"),
            new NameAndCountry("Puerto Rico", "USA"),
            new NameAndCountry("Rhode Island", "USA"),
            new NameAndCountry("South Carolina", "USA"),
            new NameAndCountry("South Dakota", "USA"),
            new NameAndCountry("Tennessee", "USA"),
            new NameAndCountry("Texas", "USA"),
            new NameAndCountry("Utah", "USA"),
            new NameAndCountry("Virginia", "USA"),
            new NameAndCountry("Virgin Islands", "USA"),
            new NameAndCountry("Vermont", "USA"),
            new NameAndCountry("Washington", "USA"),
            new NameAndCountry("Wisconsin", "USA"),
            new NameAndCountry("West Virginia", "USA"),
            new NameAndCountry("Wyoming", "USA"),

            new NameAndCountry("Northern Ireland", "United Kingdom"),
            new NameAndCountry("Wales", "United Kingdom"),
            new NameAndCountry("Scotland", "United Kingdom"),
            new NameAndCountry("England", "United Kingdom"),

            new NameAndCountry("Rivera", "Uruguay"));

    private final String _name;

    private final Country _country;

    private Region(String name, Country country)
    {
        _name = name;
        _country = country;

        for(NameAndCountry pair : _knownRegions)
            if(pair.Name.equals(name) && pair.Country.equals(country))
                return;

        throw new IllegalArgumentException("Unknown municipality: " + name + " " + country.getName());
    }

    // visitor pattern
    @Override
    public <R> R which(Cases<R> cases)
    {
        return cases.regionCase(this);
    }

    /**
     * @return the country of the region.
     */
    public Country getCountry()
    {
        return _country;
    }

    // contract from super
    @Override
    public String getName()
    {
        return _name;
    }

    /**
     * Finds the region with the given name (case-sensitive) in the given country.
     *
     * @param name the name of the region. may be {@code null}.
     * @param country the country enclosing the region being searched for. may be {@code null}.
     * @return the region with the given name. never {@code null}.
     * @throws IllegalArgumentException if no region with the given name and enclosing country is found.
     */
    public static Region get(String name, Country country)
    {
        for(NameAndCountry pair : _knownRegions)
            if(pair.Name.equals(name) && pair.Country.equals(country))
                return new Region(pair.Name, pair.Country);


        throw new IllegalArgumentException("Unknown municipality: " + name + " " + country.getName());
    }

    /**
     * Finds all regions with the given name (case-sensitive).
     *
     * @param name the name of the region. may be {@code null}.
     * @return the regions with the given name. never {@code null}.
     */
    public static Set<Region> withName(String name)
    {
        Set<Region> matches = new HashSet<>();
        for(NameAndCountry knownRegion : _knownRegions)
        {
            if(knownRegion.Name.equals(name))
                matches.add(new Region(knownRegion.Name, knownRegion.Country));
        }

        return matches;
    }

    // contract from super
    @Override
    public boolean equals(Object other)
    {
        return other instanceof Region && this.equals((Region) other);
    }

    /**
     * Tests if this region and the given region are the same region.
     *
     * @param other the candidate region. may be {@code null}.
     * @return {@code true} if the given region is non-null and has the same name as this region as tested by
     *         {@link String#equals(Object)}. Otherwise {@code false}.
     */
    public boolean equals(Region other)
    {
        if(other == null)
            return false;

        return other._name.equals(this._name);
    }

}
