package com.treangenlab.harvest_variants.ncbi.sra.geonames;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A named location in the world (e.g. a city or town) enclosed by a {@link Country} or, more specifically, a
 * {@link Region}.
 */
public class Locality
{
    private static class NameAndEnclosure
    {
        private final String Name;

        private final CountryOrRegion Enclosure;

        private NameAndEnclosure(String name, CountryOrRegion enclosure)
        {
            Name = name;
            Enclosure = enclosure;
        }
    }

    /**
     * The set of valid locations.
     */
    // ordered by Country alpha
    private static final List<NameAndEnclosure> _knownLocalities = Arrays.asList(
        new NameAndEnclosure("Amstetten",  Country.get("Austria")),
        new NameAndEnclosure("Korneuburg", Country.get("Austria")),

        new NameAndEnclosure("Dhaka", Country.get("Bangladesh")),

        new NameAndEnclosure("Gelephu", Region.get("Sarpang", Country.get("Bhutan"))),

        new NameAndEnclosure("Aao Paulo", Country.get("Brazil")),
        new NameAndEnclosure("Taquara", Region.get("Rio Grande do Sul", Country.get("Brazil"))),

        new NameAndEnclosure("Victoria", Country.get("Canada")),
        new NameAndEnclosure("Toronto",  Country.get("Canada")),

        new NameAndEnclosure("Shanghai", Country.get("China")),
        new NameAndEnclosure("Chengdu",  Country.get("China")),
        new NameAndEnclosure("Wuhan",    Country.get("China")),
        new NameAndEnclosure("Hong Kong",    Country.get("China")),

        new NameAndEnclosure("Zagreb", Country.get("Croatia")),

        new NameAndEnclosure("Lyon", Country.get("France")),

        new NameAndEnclosure("New Delhi", Country.get("India")),

        new NameAndEnclosure("Karaj", Region.get("Alborz", Country.get("Iran"))),

        new NameAndEnclosure("Dublin", Country.get("Ireland")),
        new NameAndEnclosure("Louth", Country.get("Ireland")),
        new NameAndEnclosure("Laois", Country.get("Ireland")),
        new NameAndEnclosure("Mayo", Country.get("Ireland")),
        new NameAndEnclosure("Roscommon", Country.get("Ireland")),
        new NameAndEnclosure("Galway", Country.get("Ireland")),
        new NameAndEnclosure("Cork", Country.get("Ireland")),
        new NameAndEnclosure("Kildare", Country.get("Ireland")),

        new NameAndEnclosure("Jerusalem", Country.get("Israel")),

        new NameAndEnclosure("Brescia", Region.get("Lombardia", Country.get("Italy"))),

        new NameAndEnclosure("Amman", Country.get("Jordan")),




        new NameAndEnclosure("Heinsberg", Country.get("Germany")),
        new NameAndEnclosure("Giessen",   Country.get("Germany")),
        new NameAndEnclosure("Hamburg",   Country.get("Germany")),

        new NameAndEnclosure("Tijuana", Region.get("BajaCalifornia", Country.get("Mexico"))),

        new NameAndEnclosure("Kathmandu", Country.get("Nepal")),

        new NameAndEnclosure("Leiden", Region.get("South Holland", Country.get("Netherlands"))),

        new NameAndEnclosure("Nordland", Region.get("Værøy", Country.get("Norway"))),
        new NameAndEnclosure("Oslo", Country.get("Norway")),

        new NameAndEnclosure("Callao", Country.get("Peru")),
        new NameAndEnclosure("Lima", Country.get("Peru")),

        new NameAndEnclosure("Jeddah", Country.get("Saudi Arabia")),
        new NameAndEnclosure("Madinah", Country.get("Saudi Arabia")),
        new NameAndEnclosure("Makkah", Country.get("Saudi Arabia")),
        new NameAndEnclosure("Riyadh", Country.get("Saudi Arabia")),

        new NameAndEnclosure("Nablus",                          Country.get("State of Palestine")),
        new NameAndEnclosure("Bethlehem",                       Country.get("State of Palestine")),
        new NameAndEnclosure("Jalazon",  Region.get("Ramallah", Country.get("State of Palestine"))),
        new NameAndEnclosure("Jerusalem",                       Country.get("State of Palestine")),

        new NameAndEnclosure("Stockholm", Country.get("Sweden")),

        new NameAndEnclosure("Bangkok", Country.get("Thailand")),

        new NameAndEnclosure("Tranqueras",  Region.get("Rivera", Country.get("Uruguay"))),

        new NameAndEnclosure("Denver",                 Region.get("Colorado",     Country.get("USA"))),
        new NameAndEnclosure("Smyrna",                 Region.get("Delaware",     Country.get("USA"))),
        new NameAndEnclosure("Atlanta",                Region.get("Georgia",      Country.get("USA"))),
        new NameAndEnclosure("Chicago",                Region.get("Illinois",     Country.get("USA"))),
        new NameAndEnclosure("Caddo Parish",           Region.get("Louisiana",    Country.get("USA"))),
        new NameAndEnclosure("Webster Parish",         Region.get("Louisiana",    Country.get("USA"))),
        new NameAndEnclosure("New Orleans",            Region.get("Louisiana",    Country.get("USA"))),
        new NameAndEnclosure("Hamilton",               Region.get("Montana",      Country.get("USA"))),
        new NameAndEnclosure("New York City",          Region.get("New York",     Country.get("USA"))),
        new NameAndEnclosure("Philadelphia",           Region.get("Pennsylvania", Country.get("USA"))),
        new NameAndEnclosure("Houston",                Region.get("Texas",        Country.get("USA"))),
        new NameAndEnclosure("Dane County",            Region.get("Wisconsin",    Country.get("USA"))),
        new NameAndEnclosure("Cruise_Ship_2",          Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("Cruise_Ship_1",          Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("Imperial",               Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("Napa",                   Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("Santa Clara County",     Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("Marin County",           Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("Sonoma County",          Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("San Francisco County",   Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("Alameda County",         Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("San Diego",              Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("San Luis Obispo County", Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("Madera County",          Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("Tulare County",          Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("Sonoma County",          Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("San Francisco County",   Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("Humboldt County",        Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("San Bernardino County",  Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("Tuolumne County",        Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("San Bernardino County",  Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("Contra Costa County",    Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("Ventura County",         Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("San Joaquin County",     Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("Los Angeles",            Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("Los Angeles County",     Region.get("California",   Country.get("USA"))),
        new NameAndEnclosure("Orange County",          Region.get("California",   Country.get("USA"))));

    /**
     * The name of the locality.
     */
    private final String _name;

    /**
     * The enclosure of the locality.
     */
    private final CountryOrRegion _enclosure;

    private Locality(String name, CountryOrRegion enclosure)
    {
        _name = name;
        _enclosure = enclosure;

        for(NameAndEnclosure pair : _knownLocalities)
            if(pair.Name.equals(name) && pair.Enclosure.equals(enclosure))
                return;

        throw new IllegalArgumentException("Unknown locality: " + name + " " + enclosure.getName());
    }

    /**
     * @return the name of the locality. never {@code null}.
     */
    public String getName()
    {
        return _name;
    }

    /**
     * @return the enclosure of the locality. never {@code null}.
     */
    public CountryOrRegion getEnclosure()
    {
        return _enclosure;
    }

    public Country getCountry()
    {
        return _enclosure.getCountry();
    }

    /**
     * Finds all localities with the given name irrespective of enclosure.
     *
     * @param name the name of the locality. may be {@code null}.
     * @return the localities with the given name. never {@code null}.
     */
    public static Set<Locality> withName(String name)
    {
        Set<Locality> matches = new HashSet<>();
        for(NameAndEnclosure knownLocality : _knownLocalities)
        {
            if(knownLocality.Name.equals(name))
                matches.add(new Locality(knownLocality.Name, knownLocality.Enclosure));
        }

        return matches;
    }
}
