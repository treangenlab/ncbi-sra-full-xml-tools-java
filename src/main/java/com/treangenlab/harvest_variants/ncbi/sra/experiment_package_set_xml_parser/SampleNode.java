package com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Models the {@code SAMPLE} section of an NCBI SRA Full Xml record.
 */
public interface SampleNode
{
    /**
     * @return the value of the {@code accession} attribute of this node if present. never {@code null}.
     */
    Optional<String> getAccessionId();

    /**
     * A map representation of the contents of any {@code SAMPLE_ATTRIBUTES} child of this node. For example, if the
     * xml was found to be:<br/><br/>
     *
     * &lt;SAMPLE_ATTRIBUTES&gt; <br/>
     *   &nbsp;&nbsp;&nbsp;&nbsp;&lt;SAMPLE_ATTRIBUTE&gt;<br/>
     *     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;TAG&gt;collecting institution&lt;/TAG&gt;<br/>
     *     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;VALUE&gt;not provided&lt;/VALUE&gt;<br/>
     *   &nbsp;&nbsp;&nbsp;&nbsp;&lt;/SAMPLE_ATTRIBUTE&gt;<br/>
     *   &nbsp;&nbsp;&nbsp;&nbsp;&lt;SAMPLE_ATTRIBUTE&gt;<br/>
     *     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;TAG&gt;collection date&lt;/TAG&gt;<br/>
     *     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;VALUE&gt;2022-05-02&lt;/VALUE&gt;<br/>
     *   &nbsp;&nbsp;&nbsp;&nbsp;&lt;/SAMPLE_ATTRIBUTE&gt;<br/>
     *   &nbsp;&nbsp;&nbsp;&nbsp;&lt;SAMPLE_ATTRIBUTE&gt;<br/>
     *     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;TAG&gt;key1&lt;/TAG&gt;<br/>
     *     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;VALUE&gt;value1&lt;/VALUE&gt;<br/>
     *   &nbsp;&nbsp;&nbsp;&nbsp;&lt;/SAMPLE_ATTRIBUTE&gt;<br/>
     *   &nbsp;&nbsp;&nbsp;&nbsp;&lt;SAMPLE_ATTRIBUTE&gt;<br/>
     *     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;TAG&gt;key1&lt;/TAG&gt;<br/>
     *     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;VALUE&gt;value2&lt;/VALUE&gt;<br/>
     *   &nbsp;&nbsp;&nbsp;&nbsp;&lt;/SAMPLE_ATTRIBUTE&gt;<br/>
     *   &lt;/SAMPLE_ATTRIBUTES&gt; <br/>
     *
     *<br/>
     * The returned map would be: <br/><br/>
     * {@code  {<br/> "collecting institution" -> ["not provided"],<br/>
     *           "collection date" -> ["2022-05-02"], <br/>
     *           "key1" -> ["value1", "value2"]  <br/>} }
     * @return a map representation of the sample attributes. never {@code null}.
     */
    Map<String, List<String>> getAttributes();
}
