package com.treangenlab.harvest_variants.ncbi.sra.sequenced_sample;

public interface SequencingPlatformCases<R>
{
    R illuminaCase();

    R otherCase();
}
