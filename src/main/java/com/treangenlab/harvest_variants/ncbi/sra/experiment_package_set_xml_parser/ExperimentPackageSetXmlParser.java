package com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser;

import com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser.jsoup.ExperimentPackageSetXmlParserJsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * A parser of the "Full XML" metadata file format offered by the SRA NCBI archive. For example, files like:
 *
 * {@code https://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?save=efetch&rettype=exp&db=sra&WebEnv=MCID_6296920f2d5d1d59cf18abad&query_key=1}
 */
public interface ExperimentPackageSetXmlParser
{
    /**
     * Parses the given xml string. Expected to be of the general structure
     * {@code "<?xml version="1.0"?>&lt;EXPERIMENT_PACKAGE_SET&gt;&lt;EXPERIMENT_PACKAGE&gt;...&lt;/EXPERIMENT_PACKAGE>&lt;/EXPERIMENT_PACKAGE_SET&gt;" }
     *
     * @param xml the SRA metadata file. may not be {@code null}.
     * @return a representation of the root experiment package set. never {@code null}.
     * @throws ParseException if the format of the given string is not valid XML or not does conform to the expected
     *                        structure
     */
    default ExperimentPackageSetNode parse(String xml) throws ParseException
    {
        Logger logger = LoggerFactory.getLogger(ExperimentPackageSetXmlParser.class);
        logger.debug("entering");

        ExperimentPackageSetNode node;
        try
        {
            node = parse(new ByteArrayInputStream(xml.getBytes()));
        }
        catch (IOException e) // input stream built on top in memory string should not ever throw IOException if we
        {                     // are only reading from the stream
            logger.warn(e.getMessage(), e);
            logger.debug("exiting");
            throw new RuntimeException(e);
        }

        logger.debug("exiting");
        return node;
    }

    /**
     * Parses the given xml stream. Expected to be of the general structure
     * {@code "<?xml version="1.0"?>&lt;EXPERIMENT_PACKAGE_SET&gt;&lt;EXPERIMENT_PACKAGE&gt;...&lt;/EXPERIMENT_PACKAGE>&lt;/EXPERIMENT_PACKAGE_SET&gt;" }
     *
     * @param xmlStream the UTF-8 encoded xml stream. may not be {@code null}.
     * @return a representation of the root experiment package set. never {@code null}.
     * @throws ParseException if the format of the stream's bytes is not valid XML or not does conform to the expected
     *                        structure
     */
    ExperimentPackageSetNode parse(InputStream xmlStream) throws IOException, ParseException;

    /**
     * @return a new parser of unspecified implementation. never {@code null}.
     */
    static ExperimentPackageSetXmlParser make()
    {
        return new ExperimentPackageSetXmlParserJsoup();
    }
}
