package com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser;

import java.util.List;

/**
 * Models the {@code RUN_SET} section of an NCBI SRA Full Xml record.
 */
public interface RunSetNode
{
    /**
     * @return any {@code RUN} child elements of this run set node. never {@code null}.
     */
    List<RunNode> getRuns();
}
