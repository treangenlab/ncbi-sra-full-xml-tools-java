package com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser;

import java.util.List;

/**
 * Models the {@code POOL} section of an NCBI SRA Full Xml record.
 */
public interface PoolNode
{
    /**
     * @return any {@code MEMBER} child elements of this pool node. never {@code null}.
     */
    List<MemberNode> getMembers();
}
