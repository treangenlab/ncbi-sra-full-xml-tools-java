package com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser.jsoup;

import com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * A {@code ExperimentPackageSetXmlParserTemplate} implementation that uses the JSoup library to parse experiment
 * package xml entries.
 */
public class ExperimentPackageSetXmlParserJsoup extends ExperimentPackageSetXmlParserTemplate
{
    /**
     * Logger for class.
     */
    private final Logger _logger = LoggerFactory.getLogger(ExperimentPackageSetXmlParserTemplate.class);

    // contract from super
    @Override
    protected ExperimentPackageNode parseExperimentPackageString(String xml) throws ParseException
    {
        _logger.debug("entering");

        /*
         * Check the opening and closing tag of the xml for experiment package.
         */
        if(!xml.startsWith("<EXPERIMENT_PACKAGE>"))
        {
            _logger.debug("exiting");
            throw new ParseException("given xml does not start with EXPERIMENT_PACKAGE");
        }

        if(!xml.endsWith("</EXPERIMENT_PACKAGE>"))
        {
            _logger.debug("exiting");
            throw new ParseException("given xml does not end with EXPERIMENT_PACKAGE");
        }

        /*
         * Parse the xml into Element form.
         */
        Element experimentPackage;
        {
            Document document = Jsoup.parse(xml, "", Parser.xmlParser());
            Elements docChildren = document.children();
            // assert not "<EXPERIMENT_PACKAGE></EXPERIMENT_PACKAGE><EXPERIMENT_PACKAGE></EXPERIMENT_PACKAGE>"
            if (docChildren.size() != 1)
            {
                _logger.warn("multi root xml found");
                _logger.debug("exiting");
                throw new ParseException("multiple top level elements are not allowed");
            }

            experimentPackage = docChildren.get(0);
        }

        /*
         * Extract the experiments.
         */
        List<ExperimentNode> experiments = new LinkedList<>();
        {
            List<Element> expNodes = experimentPackage.children()
                                                      .stream()
                                                      .filter((Element e) -> e.tagName().equals("EXPERIMENT"))
                                                      .collect(Collectors.toUnmodifiableList());

            for(Element node : expNodes)
                experiments.add(parseExperiment(node));
        }

        /*
         * Extract the run sets.
         */
        List<RunSetNode> runSets = new LinkedList<>();
        {
            List<Element> runSetElements = experimentPackage.children()
                                                            .stream()
                                                            .filter((Element e) -> e.tagName().equals("RUN_SET"))
                                                            .collect(Collectors.toUnmodifiableList());

            for (Element runSet : runSetElements)
                runSets.add(parseRunSet(runSet));
        }

        /*
         * Extract the run samples.
         */
        List<SampleNode> samples = new LinkedList<>();
        {

            List<Element> samplesElements = experimentPackage.children()
                                                             .stream()
                                                             .filter((Element e) -> e.tagName().equals("SAMPLE"))
                                                             .collect(Collectors.toUnmodifiableList());
            for(Element sample : samplesElements)
                samples.add(parseSample(sample));
        }

        /*
         * Extract the run pools.
         */
        List<PoolNode> pools = new LinkedList<>();
        {

            List<Element> poolElements = experimentPackage.children()
                                                          .stream()
                                                          .filter((Element e) -> e.tagName().equals("Pool"))
                                                          .collect(Collectors.toUnmodifiableList());
            for(Element pool : poolElements)
                pools.add(parsePool(pool));
        }

        /*
         * Construct package node and return.
         */
        ExperimentPackageNode packageNode = new ExperimentPackageNode()
        {
            private final List<SampleNode> _samples = Collections.unmodifiableList(samples);

            private final List<RunSetNode> _runSets = Collections.unmodifiableList(runSets);

            private final List<ExperimentNode> _experiments = Collections.unmodifiableList(experiments);

            private final List<PoolNode> _pools = Collections.unmodifiableList(pools);

            @Override
            public List<SampleNode> getSamples()
            {
                return _samples;
            }

            @Override
            public List<RunSetNode> getRunSets()
            {
                return _runSets;
            }

            @Override
            public List<ExperimentNode> getExperiments()
            {
                return _experiments;
            }

            @Override
            public List<PoolNode> getPools()
            {
                return _pools;
            }
        };

        _logger.debug("exiting");
        return packageNode;
    }

    // parse a <SAMPLE>...</SAMPLE> element
    private SampleNode parseSample(Element element)
    {
        _logger.debug("entering");

        String accession = element.attr("accession");
        Optional<String> id = accession.isEmpty() ? Optional.empty() : Optional.of(accession);

        /*
         * Strange way of encoding key value pairs, but we will parse it.
         *
         * e.g.
         *
         * <SAMPLE_ATTRIBUTES>
         *   <SAMPLE_ATTRIBUTE>
         *     <TAG>collecting institution</TAG>
         *     <VALUE>not provided</VALUE>
         *   </SAMPLE_ATTRIBUTE>
         *   <SAMPLE_ATTRIBUTE>
         *     <TAG>collection date</TAG>
         *     <VALUE>2022-05-02</VALUE>
         *   </SAMPLE_ATTRIBUTE>
         *   <SAMPLE_ATTRIBUTE>
         *     <TAG>collector name</TAG>
         *     <VALUE>Dutch COVID-19 response team</VALUE>
         *   </SAMPLE_ATTRIBUTE>
         * </SAMPLE_ATTRIBUTES>
         */
        Map<String, List<String>> attributes = new HashMap<>();
        List<Element> sampleAttributes = element.children().stream()
                                                           .filter((e) -> e.tagName().equals("SAMPLE_ATTRIBUTES"))
                                                           .collect(Collectors.toUnmodifiableList());
        for (Element a : sampleAttributes)
        {
            for (Element child : a.children())
            {
                if (!child.tagName().equals("SAMPLE_ATTRIBUTE"))
                    continue;

                List<Element> tags = child.children().stream()
                                                     .filter((e) -> e.tagName().equals("TAG"))
                                                     .collect(Collectors.toUnmodifiableList());

                List<Element> values = child.children().stream()
                                                       .filter((e) -> e.tagName().equals("VALUE"))
                                                       .collect(Collectors.toUnmodifiableList());

                int min = Math.min(tags.size(), values.size());
                for (int i = 0; i < min; i++)
                {
                    if (!attributes.containsKey(tags.get(i).text()))
                        attributes.put(tags.get(i).text(), new LinkedList<>());

                    attributes.get(tags.get(i).text()).add(values.get(i).text());
                }
            }
        }

        return new SampleNode()
        {
            @Override
            public Optional<String> getAccessionId()
            {
                return id;
            }

            @Override
            public Map<String, List<String>> getAttributes()
            {
                return Collections.unmodifiableMap(attributes);
            }
        };
    }

    // parse a <RUN_SET>...</RUN_SET> element
    private RunSetNode parseRunSet(Element runSet)
    {
        List<Element> runs = runSet.children().stream()
                                              .filter((Element e) -> e.tagName().equals("RUN"))
                                              .collect(Collectors.toUnmodifiableList());

        List<RunNode> runNodes = new LinkedList<>();
        for(Element run : runs)
            runNodes.add(parseRun(run));

        //noinspection Convert2Lambda
        return new RunSetNode()
        {
            @Override
            public List<RunNode> getRuns()
            {
                return Collections.unmodifiableList(runNodes);
            }
        };
    }

    // parse a <POOL>...</POOL> element
    private PoolNode parsePool(Element pool)
    {
        List<Element> members = pool.children().stream()
                                               .filter((Element e) -> e.tagName().equals("Member"))
                                               .collect(Collectors.toUnmodifiableList());

        List<MemberNode> memberNodes = new LinkedList<>();
        for(Element member: members)
        {
            String organismValue = member.attr("organism");
            Optional<String> organism = organismValue.isEmpty() ? Optional.empty() : Optional.of(organismValue);
            //noinspection Convert2Lambda
            memberNodes.add(new MemberNode()
            {
                @Override
                public Optional<String> getOrganism()
                {
                    return organism;
                }
            });
        }

        //noinspection Convert2Lambda
        return new PoolNode()
        {
            @Override
            public List<MemberNode> getMembers()
            {
                return Collections.unmodifiableList(memberNodes);
            }
        };

    }

    // parse an <EXPERIMENT>...</EXPERIMENT> element
    private ExperimentNode parseExperiment(Element experiment)
    {
        List<Element> platforms = experiment.children().stream()
                                                       .filter((Element e) -> e.tagName().equals("PLATFORM"))
                                                       .collect(Collectors.toUnmodifiableList());

        List<PlatformNode> platformNodes = new LinkedList<>();
        for(Element platform : platforms)
            platformNodes.add(parsePlatform(platform));

        //noinspection Convert2Lambda
        return new ExperimentNode()
        {
            @Override
            public List<PlatformNode> getPlatforms()
            {
                return platformNodes;
            }
        };
    }

    // parse a <PLATFORM>...</PLATFORM> element
    private PlatformNode parsePlatform(Element platform)
    {
        List<String> childNames = platform.children()
                                          .stream().map((Element::tagName))
                                          .collect(Collectors.toUnmodifiableList());
        //noinspection Convert2Lambda
        return new PlatformNode()
        {
            @Override
            public List<String> getChildElementNames()
            {
                return childNames;
            }
        };
    }

    // parse a <RUN>...</RUN> element
    private RunNode parseRun(Element run)
    {
        String id = run.attr("accession");
        String totalBases = run.attr("total_bases");
        return new RunNode()
        {
            @Override
            public Optional<String> getAccessionId()
            {
                return totalBases.isEmpty() ? Optional.empty() : Optional.of(id);
            }

            @Override
            public Optional<String> getTotalBases()
            {
                return totalBases.isEmpty() ? Optional.empty() : Optional.of(totalBases);
            }
        };
    }
}
