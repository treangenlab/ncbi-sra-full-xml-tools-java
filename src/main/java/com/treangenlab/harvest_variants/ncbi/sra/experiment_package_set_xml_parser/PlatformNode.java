package com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser;

import java.util.List;

/**
 * Models the {@code PLATFORM} section of an NCBI SRA Full Xml record.
 */
public interface PlatformNode
{
    /**
     * @return the name of each child element node of this platform node. never {@code null}.
     */
    List<String> getChildElementNames();
}
