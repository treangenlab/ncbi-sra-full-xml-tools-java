package com.treangenlab.harvest_variants.ncbi.sra.geonames;

/**
 * Thrown to indicate an error occurred during extraction.
 */
public class GeoExtractionException extends Exception
{
    public GeoExtractionException(String message)
    {
        super(message);
    }
}
