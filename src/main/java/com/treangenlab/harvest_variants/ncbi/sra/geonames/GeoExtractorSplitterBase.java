package com.treangenlab.harvest_variants.ncbi.sra.geonames;

import java.util.List;

/**
 * Optional base class for {@code GeoExtractor} implementations that use a {@link GeoTagSplitter} to produce name parts.
 */
public abstract class GeoExtractorSplitterBase implements GeoExtractor
{
    private final GeoTagSplitter _splitter;

    public GeoExtractorSplitterBase(GeoTagSplitter splitter)
    {
        if(splitter == null)
            throw new NullPointerException("splitter");

        _splitter = splitter;
    }

    // contract from super
    @Override
    public Result extract(String geoString) throws GeoExtractionException
    {
        return extract(_splitter.split(geoString));
    }

    /**
     * Extracts locality information from the given strings in an implementation specific way.
     *
     * @param geoNameParts the source of geographic information.
     * @return the location information contained in {@code geoString}
     * @throws GeoExtractionException if there is an implementation specific error in extracting locality information.
     */
    protected abstract Result extract(List<String> geoNameParts) throws GeoExtractionException;
}
