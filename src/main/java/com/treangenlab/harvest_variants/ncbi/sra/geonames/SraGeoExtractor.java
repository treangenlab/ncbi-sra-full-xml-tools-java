package com.treangenlab.harvest_variants.ncbi.sra.geonames;

import java.util.ArrayList;
import java.util.List;

/**
 * A {@code GeoExtractor} tailored to the naming trends of geography tags in sra xml.
 */
public class SraGeoExtractor extends GeoExtractorSplitterBase
{
    /**
     * Flag to indicate whether, during extraction, if it is legal to return a detected country, if the country is the
     * only name detected, in the context of having additional unused name parts.
     */
    private final boolean _allowCountryOnlyReturnWhenNotAllNamePartsUsed;

    public SraGeoExtractor(boolean allowCountryOnlyGeonameResolution)
    {
        super(new SraGeoTagSplitter());
        _allowCountryOnlyReturnWhenNotAllNamePartsUsed = allowCountryOnlyGeonameResolution;
    }

    /**
     * Attempts to determine as precise a location as possible given a multi-word description of a place. Unless this
     * extractor is constructed using the {@code _allowCountryOnlyReturnWhenNotAllNamePartsUsed} flag set to
     * {@code true}, each word in the given list of words must contribute to the resolution of the returned location or
     * else a {@code GeoExtractionException} is thrown.
     *
     * If the {@code _allowCountryOnlyReturnWhenNotAllNamePartsUsed} flag set to {@code true}, this method is allowed
     * to return a result with only a country component even if the given list of names contains more names than just
     * the name of the country.
     *
     * @param nameParts a multi-word description of a geographic location. may not be {@code null}.
     * @return the location. never {@code null}.
     * @throws GeoExtractionException if there is a problem performing resolution
     */
    protected Result extract(List<String> nameParts) throws GeoExtractionException
    {
        if(nameParts.isEmpty())
            return Result.Empty;

        // Country is the largest encompassing geoname in our Result model, so we can ignore super-country names.
        List<String> namePartsExcludingContinents = new ArrayList<>(nameParts);
        namePartsExcludingContinents.remove("North America");
        namePartsExcludingContinents.remove("South America");
        namePartsExcludingContinents.remove("Europe");
        namePartsExcludingContinents.remove("Africa");
        namePartsExcludingContinents.remove("Asia");
        namePartsExcludingContinents.remove("Australia/Oceania");
        namePartsExcludingContinents.remove("Antarctica");

        if(namePartsExcludingContinents.isEmpty())
            throw new GeoExtractionException("Could not determine country: " + nameParts);

        Locality locality = determineLocality(namePartsExcludingContinents);
        if(locality != null) // if we determined the locality and country described (knowing locality implies country)
        {                    // and possibly determined the region
            return extractHelpWhenLocalityKnown(locality, namePartsExcludingContinents, nameParts);
        }

        // strings that have not yet contributed to the returned result
        List<String> unusedParts = new ArrayList<>(namePartsExcludingContinents);

        Region region = determineRegion(namePartsExcludingContinents);
        if(region != null) // if we determined both the region and country described, but not the locality
        {
            unusedParts.remove(region.getName());

            while(unusedParts.contains(region.getCountry().getName())) // country names are often repeated in sra xml
                unusedParts.remove(region.getCountry().getName());     // remove each duplication

            if(unusedParts.size() == 0)
                return Result.make(region.getCountry(), region, null); // locality already asserted to be null above

            throw new GeoExtractionException("Could not map all parts to geography: " + nameParts);
        }

        // manually observed special case in sra xml
        if(namePartsExcludingContinents.size() == 6 && namePartsExcludingContinents.contains("Marin Counties"))
        {
            Region california = Region.get("California", Country.get("USA"));
            return Result.make(california.getCountry(), california, null); // locality already asserted to be null above
        }

        /*
         * At this point we know we can't resolve the locality or region. Try to get just country.
         */

        Country country = null;
        for(String part : namePartsExcludingContinents)
        {
            try
            {
                country = Country.get(part);
                break;
            }
            catch (IllegalArgumentException e)
            {
                continue;
            }
        }

        if(country == null)
            throw new GeoExtractionException("Could not determine country: " + nameParts);

        while(unusedParts.contains(country.getName())) // country names are often repeated in sra xml
            unusedParts.remove(country.getName()); // remove each duplication

        if(unusedParts.size() > 0 && !_allowCountryOnlyReturnWhenNotAllNamePartsUsed)
            throw new GeoExtractionException("Could not map all parts to geography: " + nameParts);

        return Result.make(country, null, null); // region and locality already asserted to be null above
    }

    private Result extractHelpWhenLocalityKnown(Locality locality, List<String> namePartsExcludingContinent,
                                                List<String> nameParts) throws GeoExtractionException
    {
        // strings that have not yet contributed to the returned result
        List<String> unusedParts = new ArrayList<>(namePartsExcludingContinent);
        unusedParts.remove(locality.getName());

        Region region = locality.getEnclosure().which(new CountryOrRegion.Cases<Region>()
        {
            @Override
            public Region regionCase(Region region)
            {
                return region;
            }

            @Override
            public Region countryCase(Country country)
            {
                return null;
            }
        });

        if(region != null)
            unusedParts.remove(region.getName());

        Country country = locality.getCountry();

        while(unusedParts.contains(country.getName())) // country names are often repeated in sra xml
            unusedParts.remove(country.getName()); // remove each duplication

        if(unusedParts.size() == 0)
            return Result.make(country, region, locality);

        throw new GeoExtractionException("Could not map all parts to geography: " + nameParts);
    }

    private Region determineRegion(List<String> nameParts)
    {
        for(String part : nameParts)
        {
            for(Region proposedRegion : Region.withName(part))
            {
                // if proposedRegion and nameParts both encode exactly the same two names (region name and country name)
                if(determineRegionHelp(proposedRegion, nameParts))
                    return proposedRegion;
            }
        }

        return null;
    }

    // return true if nameParts contains exactly the name of the given region and that region's country.
    //
    // n.b. this method is only called from a context where we know we can't resolve the locality, so there is no risk
    // of returning false because nameParts contains a locality that we could later resolve, remove from the list,
    // and then return true on the same given proposedRegion for which we previously returned false
    private boolean determineRegionHelp(Region proposedRegion, List<String> nameParts)
    {
        List<String> unusedParts = new ArrayList<>(nameParts);
        boolean matchedOnRegion = unusedParts.remove(proposedRegion.getName());
        boolean matchedOnCountry = unusedParts.remove(proposedRegion.getCountry().getName());
        return matchedOnRegion && matchedOnCountry && unusedParts.isEmpty();
    }

    private Locality determineLocality(List<String> nameParts)
    {
        for(String part : nameParts)
        {
            for(Locality proposedLocality : Locality.withName(part))
            {
                if(determineLocalityHelp(proposedLocality, nameParts))
                    return proposedLocality;
            }
        }

        return null;
    }

    private boolean determineLocalityHelp(Locality proposedLocality, List<String> nameParts)
    {
        List<String> unusedParts = new ArrayList<>(nameParts);
        boolean matchedOnLocality = unusedParts.remove(proposedLocality.getName());
        boolean allSuperAreasFound = proposedLocality.getEnclosure().which(new CountryOrRegion.Cases<Boolean>()
        {
            @Override
            public Boolean regionCase(Region region)
            {
                boolean matchedOnRegion = unusedParts.remove(region.getName());
                boolean matchedOnCountry = unusedParts.remove(region.getCountry().getName());
                return matchedOnRegion && matchedOnCountry;
            }

            @Override
            public Boolean countryCase(Country country)
            {
                return unusedParts.remove(country.getName());
            }
        });

        if(matchedOnLocality && allSuperAreasFound) // if we covered all name parts using all the parts of a locality
        {
            // Sometimes a locality is listed in the name parts but the country name is duplicated. If so, don't let
            // only a lingering country name prevent locality usage.
            if(!unusedParts.isEmpty())
            {
                String countryName = proposedLocality.getEnclosure().getCountry().getName();
                while (unusedParts.contains(countryName))
                    unusedParts.remove(countryName);
            }
        }

        return matchedOnLocality && allSuperAreasFound && unusedParts.isEmpty();
    }
}