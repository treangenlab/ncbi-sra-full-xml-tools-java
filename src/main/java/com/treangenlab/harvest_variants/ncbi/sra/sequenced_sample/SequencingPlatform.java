package com.treangenlab.harvest_variants.ncbi.sra.sequenced_sample;

public enum SequencingPlatform
{
    Illumina
    {
        @Override
        public <R> R which(SequencingPlatformCases<R> cases)
        {
            return cases.illuminaCase();
        }
    },

    Other
    {
        @Override
        public <R> R which(SequencingPlatformCases<R> cases)
        {
            return cases.otherCase();
        }
    };

    public abstract <R> R which(SequencingPlatformCases<R> cases);
}
