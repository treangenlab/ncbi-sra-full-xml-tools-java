package com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser;

/**
 * Thrown to indicate that a character sequence did not conform to the expected format.
 */
public class ParseException extends Exception
{
    /**
     * Constructs a new exception.
     *
     * @param message the detail message. The detail message is saved for later retrieval by the {@link #getMessage()}
     *                method. may be {@code null}.
     */
    public ParseException(String message)
    {
        super(message);
    }
}
