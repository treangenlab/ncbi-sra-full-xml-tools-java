package com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser;

import java.util.Optional;

/**
 * Models the {@code <MEMBER>} section of an NCBI SRA Full Xml record.
 */
public interface MemberNode
{
    /**
     * @return the value of the {@code organism} attribute of this member node, if present. never @{code null}.
     */
    Optional<String> getOrganism();
}
