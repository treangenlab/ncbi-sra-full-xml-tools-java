package com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser;

import java.util.Optional;

/**
 * Models the {@code RUN} section of an NCBI SRA Full Xml record.
 */
public interface RunNode
{
    /**
     * @return the value of the {@code accession} attribute of this node if present. never {@code null}.
     */
    Optional<String> getAccessionId();

    /**
     * @return the value of the {@code total_bases} attribute of this node if present. never {@code null}.
     */
    Optional<String> getTotalBases();
}
