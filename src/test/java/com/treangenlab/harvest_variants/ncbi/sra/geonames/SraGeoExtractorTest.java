package com.treangenlab.harvest_variants.ncbi.sra.geonames;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

public class SraGeoExtractorTest
{
    @Test
    public void testExtract_India() throws GeoExtractionException
    {
        GeoExtractor.Result result = new SraGeoExtractor(false).extract("India");
        Assert.assertEquals(Country.get("India"), result.getCountry().orElse(null));
        Assert.assertTrue(result.getRegion().isEmpty());
        Assert.assertTrue(result.getLocality().isEmpty());
    }

    @Test
    public void testExtract_USA_Colorado() throws GeoExtractionException
    {
        GeoExtractor.Result result = new SraGeoExtractor(false).extract("USA: Colorado");
        Assert.assertEquals(Country.get("USA"), result.getCountry().orElse(null));
        Assert.assertEquals("Colorado", result.getRegion().orElse(null).getName());
        Assert.assertTrue(result.getLocality().isEmpty());
    }

    @Test
    public void testExtract_USA_Denver_Colorado() throws GeoExtractionException
    {
        GeoExtractor.Result result = new SraGeoExtractor(false).extract("USA: Denver, Colorado");
        Assert.assertEquals(Country.get("USA"), result.getCountry().orElse(null));
        Assert.assertEquals("Colorado", result.getRegion().orElse(null).getName());
        Assert.assertEquals("Denver", result.getLocality().orElse(null).getName());
    }

    @Test
    public void testExtract_Jordan_Amman() throws GeoExtractionException
    {
        GeoExtractor.Result result = new SraGeoExtractor(false).extract("Jordan: Amman");
        Assert.assertEquals(Country.get("Jordan"), result.getCountry().orElse(null));
        Assert.assertTrue(result.getRegion().isEmpty());
        Assert.assertEquals("Amman", result.getLocality().orElse(null).getName());
    }

    @Test(expected = GeoExtractionException.class)
    public void testUnusedCountry() throws GeoExtractionException
    {
        GeoExtractor.Result result = new SraGeoExtractor(false).extract("Wakanda: Denver, Colorado");
        result.getCountry(); // force lazy evaluation to determine Wakanda is not known.
    }

    @Test(expected = GeoExtractionException.class)
    public void testUnusedRegion() throws GeoExtractionException
    {
        GeoExtractor.Result result = new SraGeoExtractor(false).extract("USA: Denver, Region");
        result.getCountry(); // force lazy evaluation
    }

    @Test(expected = GeoExtractionException.class)
    public void testUnusedLocality() throws GeoExtractionException
    {
        GeoExtractor.Result result = new SraGeoExtractor(false).extract("USA: Locality, Colorado");
        result.getCountry(); // force lazy evaluation
    }

    @Test(expected = GeoExtractionException.class)
    public void testUnknownRegionNoCountryOnly() throws GeoExtractionException
    {
        GeoExtractor.Result result = new SraGeoExtractor(false).extract("United Kingdom: NORFOLK");
        result.getCountry(); // force lazy evaluation
    }

    @Test
    public void testUnknownRegionCountryOnly() throws GeoExtractionException
    {
        GeoExtractor.Result result = new SraGeoExtractor(true).extract("United Kingdom: NORFOLK");
        Assert.assertEquals(Country.get("United Kingdom"), result.getCountry().orElse(null));
    }
}
