package com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser.jsoup;

import com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser.ExperimentPackageSetXmlParser;
import com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser.ExperimentPackageSetXmlParserTest;
import com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser.ParseException;
import org.junit.Test;

public class ExperimentPackageSetXmlParserJsoupTest extends ExperimentPackageSetXmlParserTest
{
    @Override
    protected ExperimentPackageSetXmlParser makeParser()
    {
        return new ExperimentPackageSetXmlParserJsoup();
    }

    @Test(expected = ParseException.class)
    public void testParseExperimentPackage_MultiChild() throws ParseException
    {
        String packageXml = "<EXPERIMENT_PACKAGE></EXPERIMENT_PACKAGE><EXPERIMENT_PACKAGE></EXPERIMENT_PACKAGE>";
        new ExperimentPackageSetXmlParserJsoup().parseExperimentPackageString(packageXml);
    }

    @Test(expected = ParseException.class)
    public void testParseExperimentPackage_BadStart() throws ParseException
    {
        String packageXml = "<Dog></EXPERIMENT_PACKAGE><EXPERIMENT_PACKAGE></EXPERIMENT_PACKAGE>";
        new ExperimentPackageSetXmlParserJsoup().parseExperimentPackageString(packageXml);
    }

    @Test(expected = ParseException.class)
    public void testParseExperimentPackage_BadEnd() throws ParseException
    {
        String packageXml = "<EXPERIMENT_PACKAGE></EXPERIMENT_PACKAGE><EXPERIMENT_PACKAGE></Cat>";
        new ExperimentPackageSetXmlParserJsoup().parseExperimentPackageString(packageXml);
    }



}
