package com.treangenlab.harvest_variants.ncbi.sra.geonames;

import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

public class LocalityTest
{
    @Test
    public void testWithName()
    {
        Set<Locality> matches =  Locality.withName("Denver");

        Assert.assertEquals(1, matches.size());

        Locality match = matches.iterator().next();

        Assert.assertEquals("Denver", match.getName());
        Assert.assertEquals("USA", match.getEnclosure().getCountry().getName());
    }
}
