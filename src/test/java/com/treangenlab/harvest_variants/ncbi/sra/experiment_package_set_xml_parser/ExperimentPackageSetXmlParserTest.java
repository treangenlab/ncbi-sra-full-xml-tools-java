package com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.List;

public abstract class ExperimentPackageSetXmlParserTest
{
    protected abstract ExperimentPackageSetXmlParser makeParser();

    @Test(expected = ParseException.class)
    public void testParse_BadStartChars1() throws ParseException
    {
        makeParser().parse("some random start");
    }

    @Test(expected = ParseException.class)
    public void testParse_BadStartChars2() throws ParseException
    {
        makeParser().parse("<?xml version=\"1.0\"?>\n<Dog></EXPERIMENT_PACKAGE_SET>");
    }

    @Test(expected = ParseException.class)
    public void testParse_BadFinalChars() throws ParseException
    {
        makeParser().parse("<?xml version=\"1.0\"?>\n<EXPERIMENT_PACKAGE_SET></wrong_close>");
    }

    @Test(expected = ParseException.class)
    public void testParse_TrailingChars() throws ParseException
    {
        makeParser().parse("<?xml version=\"1.0\"?>\n<EXPERIMENT_PACKAGE_SET></EXPERIMENT_PACKAGE_SET>trail");
    }

    @Test
    public void testParse_ValidXml1() throws ParseException
    {
        ExperimentPackageSetNode packageSet = makeParser().parse(_validXml1);
        List<ExperimentPackageNode> packages  = packageSet.getPackages();

        Assert.assertEquals(3, packages.size());

        ExperimentPackageNode firstPackage  = packages.get(0);
        ExperimentPackageNode secondPackage = packages.get(1);
        ExperimentPackageNode thirdPackage  = packages.get(2);

        String virus = "Severe acute respiratory syndrome coronavirus 2";
        String platform = "OXFORD_NANOPORE";
        assertExpectedPackage(firstPackage,  "SRR10948550", "SRS6014638", virus, platform,  146271136,
                              new HashMap<>() {{ put("strain", "Betacoronavirus"); put("collection_date", "2020-01");
                                                 put("geo_loc_name", "China"); put("host", "Homo sapiens");
                                                 put("sample_type", "Nasopharyngeal Swab");
                                                 put("BioSampleModel", "Microbe, viral or environmental"); }});

        assertExpectedPackage(secondPackage, "SRR10948474", "SRS6048919", virus, platform, 284575255,
                              new HashMap<>() {{ put("isolate", "Sputum"); put("collection_date", "2020-01");
                                                 put("geo_loc_name", "China"); put("host", "Homo sapiens");
                                                 put("sample_type", "Sputum");
                                                 put("BioSampleModel", "Microbe, viral or environmental"); }});

        assertExpectedPackage(thirdPackage,  "SRR10902284", "SRS6014638", virus, platform,  90569410,
                              new HashMap<>() {{ put("strain", "Betacoronavirus"); put("collection_date", "2020-01");
                                                 put("geo_loc_name", "China"); put("host", "Homo sapiens");
                                                 put("sample_type", "Nasopharyngeal Swab");
                                                 put("BioSampleModel", "Microbe, viral or environmental"); }});


    }

    private void assertExpectedPackage(ExperimentPackageNode packageNode, String runAccessionId,
                                       String sampleAccessionId, String organismName, String platformName,
                                       int totalBaseCount, Map<String,String> sampleAttributes)
    {
        List<PoolNode> pools = packageNode.getPools();
        Assert.assertEquals(1, pools.size());

        PoolNode pool = pools.get(0);

        List<MemberNode> members = pool.getMembers();
        Assert.assertEquals(1, members.size());

        MemberNode member = members.get(0);

        Assert.assertEquals(organismName, member.getOrganism().orElseThrow());

        List<ExperimentNode> experiments = packageNode.getExperiments();
        Assert.assertEquals(1, experiments.size());

        ExperimentNode experiment = experiments.get(0);

        List<PlatformNode> platforms = experiment.getPlatforms();
        Assert.assertEquals(1, platforms.size());

        PlatformNode platform = platforms.get(0);

        List<String> platformChildElementNames = platform.getChildElementNames();
        Assert.assertEquals(1, platformChildElementNames.size());

        String platformChildElementName = platformChildElementNames.get(0);
        Assert.assertEquals(platformName, platformChildElementName);

        List<RunSetNode> runSets = packageNode.getRunSets();
        Assert.assertEquals(1, runSets.size());

        RunSetNode runSet = runSets.get(0);

        List<RunNode> runs = runSet.getRuns();
        Assert.assertEquals(1, runs.size());

        RunNode run = runs.get(0);
        Assert.assertEquals(runAccessionId, run.getAccessionId().orElseThrow());
        Assert.assertEquals(totalBaseCount + "", run.getTotalBases().orElseThrow());

        List<SampleNode> sampleNodes = packageNode.getSamples();
        Assert.assertEquals(1, sampleNodes.size());

        SampleNode sample = sampleNodes.get(0);
        Assert.assertEquals(sampleAccessionId, sample.getAccessionId().orElseThrow());

        Map<String, List<String>> attributes = sample.getAttributes();
        for(List<String> values : attributes.values())
            Assert.assertEquals(1, values.size());

        Assert.assertEquals(sampleAttributes.size(), attributes.size());

        for(String expectedKey : sampleAttributes.keySet())
        {
            String expectedValue = sampleAttributes.get(expectedKey);
            Assert.assertEquals(expectedValue, attributes.get(expectedKey).get(0));
        }

    }

    @Test
    public void testParse_SraNonPrettyPrintNoException() throws ParseException
    {
        makeParser().parse(_validXml2);
    }

    @SuppressWarnings("FieldCanBeLocal") // would make method very hard to read.
    private final String _validXml1 = "<?xml version=\"1.0\"?>\n" +
            "<EXPERIMENT_PACKAGE_SET>\n" +
            "  <EXPERIMENT_PACKAGE>\n" +
            "    <EXPERIMENT accession=\"SRX7615629\" alias=\"HKU-SZ-002a\">\n" +
            "      <IDENTIFIERS>\n" +
            "        <PRIMARY_ID>SRX7615629</PRIMARY_ID>\n" +
            "      </IDENTIFIERS>\n" +
            "      <TITLE>Whole genome sequencing of BetaCoronavirus</TITLE>\n" +
            "      <STUDY_REF accession=\"SRP242169\">\n" +
            "        <IDENTIFIERS>\n" +
            "          <PRIMARY_ID>SRP242169</PRIMARY_ID>\n" +
            "          <EXTERNAL_ID namespace=\"BioProject\">PRJNA601630</EXTERNAL_ID>\n" +
            "        </IDENTIFIERS>\n" +
            "      </STUDY_REF>\n" +
            "      <DESIGN>\n" +
            "        <DESIGN_DESCRIPTION>Whole genome sequencing of Betacoronavirus</DESIGN_DESCRIPTION>\n" +
            "        <SAMPLE_DESCRIPTOR accession=\"SRS6014638\">\n" +
            "          <IDENTIFIERS>\n" +
            "            <PRIMARY_ID>SRS6014638</PRIMARY_ID>\n" +
            "            <EXTERNAL_ID namespace=\"BioSample\">SAMN13871323</EXTERNAL_ID>\n" +
            "          </IDENTIFIERS>\n" +
            "        </SAMPLE_DESCRIPTOR>\n" +
            "        <LIBRARY_DESCRIPTOR>\n" +
            "          <LIBRARY_NAME>HKU-SZ-002a</LIBRARY_NAME>\n" +
            "          <LIBRARY_STRATEGY>RNA-Seq</LIBRARY_STRATEGY>\n" +
            "          <LIBRARY_SOURCE>GENOMIC</LIBRARY_SOURCE>\n" +
            "          <LIBRARY_SELECTION>RANDOM</LIBRARY_SELECTION>\n" +
            "          <LIBRARY_LAYOUT>\n" +
            "            <SINGLE/>\n" +
            "          </LIBRARY_LAYOUT>\n" +
            "        </LIBRARY_DESCRIPTOR>\n" +
            "      </DESIGN>\n" +
            "      <PLATFORM>\n" +
            "        <OXFORD_NANOPORE>\n" +
            "          <INSTRUMENT_MODEL>MinION</INSTRUMENT_MODEL>\n" +
            "        </OXFORD_NANOPORE>\n" +
            "      </PLATFORM>\n" +
            "    </EXPERIMENT>\n" +
            "    <SUBMISSION lab_name=\"NA\" center_name=\"HKU-Shenzhen Hospital\" accession=\"SRA1029822\" alias=\"SUB6866118\">\n" +
            "      <IDENTIFIERS>\n" +
            "        <PRIMARY_ID>SRA1029822</PRIMARY_ID>\n" +
            "      </IDENTIFIERS>\n" +
            "    </SUBMISSION>\n" +
            "    <Organization type=\"institute\">\n" +
            "      <Name>HKU-Shenzhen Hospital</Name>\n" +
            "      <Address postal_code=\"518053\">\n" +
            "        <Department>NA</Department>\n" +
            "        <Institution>HKU-Shenzhen Hospital</Institution>\n" +
            "        <Street>1, Haiyuan 1st Road, Futian District</Street>\n" +
            "        <City>Shenzhen</City>\n" +
            "        <Country>China</Country>\n" +
            "      </Address>\n" +
            "      <Contact email=\"jdip1007@connect.hku.hk\">\n" +
            "        <Address postal_code=\"518053\">\n" +
            "          <Department>NA</Department>\n" +
            "          <Institution>HKU-Shenzhen Hospital</Institution>\n" +
            "          <Street>1, Haiyuan 1st Road, Futian District</Street>\n" +
            "          <City>Shenzhen</City>\n" +
            "          <Country>China</Country>\n" +
            "        </Address>\n" +
            "        <Name>\n" +
            "          <First>Jonathan</First>\n" +
            "          <Last>Ip</Last>\n" +
            "          <Middle>Daniel</Middle>\n" +
            "        </Name>\n" +
            "      </Contact>\n" +
            "    </Organization>\n" +
            "    <STUDY center_name=\"BioProject\" alias=\"PRJNA601630\" accession=\"SRP242169\">\n" +
            "      <IDENTIFIERS>\n" +
            "        <PRIMARY_ID>SRP242169</PRIMARY_ID>\n" +
            "        <EXTERNAL_ID namespace=\"BioProject\" label=\"primary\">PRJNA601630</EXTERNAL_ID>\n" +
            "      </IDENTIFIERS>\n" +
            "      <DESCRIPTOR>\n" +
            "        <STUDY_TITLE>Severe acute respiratory syndrome coronavirus 2 Genome sequencing</STUDY_TITLE>\n" +
            "        <STUDY_TYPE existing_study_type=\"Whole Genome Sequencing\"/>\n" +
            "        <STUDY_ABSTRACT>Whole genome sequencing from a patient's specimen</STUDY_ABSTRACT>\n" +
            "        <CENTER_PROJECT_NAME>Wuhan seafood market pneumonia virus</CENTER_PROJECT_NAME>\n" +
            "      </DESCRIPTOR>\n" +
            "    </STUDY>\n" +
            "    <SAMPLE alias=\"HKU-SZ-002a\" accession=\"SRS6014638\">\n" +
            "      <IDENTIFIERS>\n" +
            "        <PRIMARY_ID>SRS6014638</PRIMARY_ID>\n" +
            "        <EXTERNAL_ID namespace=\"BioSample\">SAMN13871323</EXTERNAL_ID>\n" +
            "      </IDENTIFIERS>\n" +
            "      <TITLE>Microbe sample from Coronavirus</TITLE>\n" +
            "      <SAMPLE_NAME>\n" +
            "        <TAXON_ID>2697049</TAXON_ID>\n" +
            "        <SCIENTIFIC_NAME>Severe acute respiratory syndrome coronavirus 2</SCIENTIFIC_NAME>\n" +
            "      </SAMPLE_NAME>\n" +
            "      <SAMPLE_LINKS>\n" +
            "        <SAMPLE_LINK>\n" +
            "          <XREF_LINK>\n" +
            "            <DB>bioproject</DB>\n" +
            "            <ID>601630</ID>\n" +
            "            <LABEL>PRJNA601630</LABEL>\n" +
            "          </XREF_LINK>\n" +
            "        </SAMPLE_LINK>\n" +
            "      </SAMPLE_LINKS>\n" +
            "      <SAMPLE_ATTRIBUTES>\n" +
            "        <SAMPLE_ATTRIBUTE>\n" +
            "          <TAG>strain</TAG>\n" +
            "          <VALUE>Betacoronavirus</VALUE>\n" +
            "        </SAMPLE_ATTRIBUTE>\n" +
            "        <SAMPLE_ATTRIBUTE>\n" +
            "          <TAG>collection_date</TAG>\n" +
            "          <VALUE>2020-01</VALUE>\n" +
            "        </SAMPLE_ATTRIBUTE>\n" +
            "        <SAMPLE_ATTRIBUTE>\n" +
            "          <TAG>geo_loc_name</TAG>\n" +
            "          <VALUE>China</VALUE>\n" +
            "        </SAMPLE_ATTRIBUTE>\n" +
            "        <SAMPLE_ATTRIBUTE>\n" +
            "          <TAG>host</TAG>\n" +
            "          <VALUE>Homo sapiens</VALUE>\n" +
            "        </SAMPLE_ATTRIBUTE>\n" +
            "        <SAMPLE_ATTRIBUTE>\n" +
            "          <TAG>sample_type</TAG>\n" +
            "          <VALUE>Nasopharyngeal Swab</VALUE>\n" +
            "        </SAMPLE_ATTRIBUTE>\n" +
            "        <SAMPLE_ATTRIBUTE>\n" +
            "          <TAG>BioSampleModel</TAG>\n" +
            "          <VALUE>Microbe, viral or environmental</VALUE>\n" +
            "        </SAMPLE_ATTRIBUTE>\n" +
            "      </SAMPLE_ATTRIBUTES>\n" +
            "    </SAMPLE>\n" +
            "    <Pool>\n" +
            "      <Member member_name=\"\" accession=\"SRS6014638\" sample_name=\"HKU-SZ-002a\" sample_title=\"Microbe sample from Coronavirus\" spots=\"425717\" bases=\"146271136\" tax_id=\"2697049\" organism=\"Severe acute respiratory syndrome coronavirus 2\">\n" +
            "        <IDENTIFIERS>\n" +
            "          <PRIMARY_ID>SRS6014638</PRIMARY_ID>\n" +
            "          <EXTERNAL_ID namespace=\"BioSample\">SAMN13871323</EXTERNAL_ID>\n" +
            "        </IDENTIFIERS>\n" +
            "      </Member>\n" +
            "    </Pool>\n" +
            "    <RUN_SET>\n" +
            "      <RUN accession=\"SRR10948550\" alias=\"HKU-SZ-002a.fastq\" total_spots=\"425717\" total_bases=\"146271136\" size=\"132697821\" load_done=\"true\" published=\"2020-01-26 11:20:39\" is_public=\"true\" cluster_name=\"public\" static_data_available=\"1\" filtered_data_available=\"true\">\n" +
            "        <IDENTIFIERS>\n" +
            "          <PRIMARY_ID>SRR10948550</PRIMARY_ID>\n" +
            "        </IDENTIFIERS>\n" +
            "        <EXPERIMENT_REF accession=\"SRX7615629\">\n" +
            "          <IDENTIFIERS/>\n" +
            "        </EXPERIMENT_REF>\n" +
            "        <Pool>\n" +
            "          <Member member_name=\"\" accession=\"SRS6014638\" sample_name=\"HKU-SZ-002a\" sample_title=\"Microbe sample from Coronavirus\" spots=\"425717\" bases=\"146271136\" tax_id=\"2697049\" organism=\"Severe acute respiratory syndrome coronavirus 2\">\n" +
            "            <IDENTIFIERS>\n" +
            "              <PRIMARY_ID>SRS6014638</PRIMARY_ID>\n" +
            "              <EXTERNAL_ID namespace=\"BioSample\">SAMN13871323</EXTERNAL_ID>\n" +
            "            </IDENTIFIERS>\n" +
            "          </Member>\n" +
            "        </Pool>\n" +
            "        <SRAFiles>\n" +
            "          <SRAFile cluster=\"public\" filename=\"SRR10948550\" url=\"https://sra-downloadb.be-md.ncbi.nlm.nih.gov/sos3/sra-pub-run-21/SRR10948550/SRR10948550.2\" size=\"132699606\" date=\"2020-01-28 22:02:09\" md5=\"2377d422be45d82de70a972d3dac6d70\" semantic_name=\"run\" supertype=\"Primary ETL\" sratoolkit=\"1\">\n" +
            "            <Alternatives url=\"https://sra-pub-sars-cov2.s3.amazonaws.com/run/SRR10948550/SRR10948550\" free_egress=\"worldwide\" access_type=\"anonymous\" org=\"AWS\"/>\n" +
            "            <Alternatives url=\"https://sra-downloadb.be-md.ncbi.nlm.nih.gov/sos3/sra-pub-run-21/SRR10948550/SRR10948550.2\" free_egress=\"worldwide\" access_type=\"anonymous\" org=\"NCBI\"/>\n" +
            "            <Alternatives url=\"https://storage.googleapis.com/nih-sequence-read-archive/run/SRR10948550/SRR10948550\" free_egress=\"worldwide\" access_type=\"anonymous\" org=\"GCP\"/>\n" +
            "          </SRAFile>\n" +
            "        </SRAFiles>\n" +
            "        <CloudFiles>\n" +
            "          <CloudFile filetype=\"run\" provider=\"gs\" location=\"gs.US\"/>\n" +
            "          <CloudFile filetype=\"run\" provider=\"s3\" location=\"s3.us-east-1\"/>\n" +
            "        </CloudFiles>\n" +
            "        <Statistics nreads=\"1\" nspots=\"425717\">\n" +
            "          <Read index=\"0\" count=\"425717\" average=\"343.59\" stdev=\"109.44\"/>\n" +
            "        </Statistics>\n" +
            "        <Databases>\n" +
            "          <Database>\n" +
            "            <Table name=\"SEQUENCE\">\n" +
            "              <Statistics source=\"meta\">\n" +
            "                <Rows count=\"425717\"/>\n" +
            "                <Elements count=\"146271136\"/>\n" +
            "              </Statistics>\n" +
            "            </Table>\n" +
            "          </Database>\n" +
            "        </Databases>\n" +
            "        <Bases cs_native=\"false\" count=\"146271136\">\n" +
            "          <Base value=\"A\" count=\"35173112\"/>\n" +
            "          <Base value=\"C\" count=\"36183390\"/>\n" +
            "          <Base value=\"G\" count=\"36344022\"/>\n" +
            "          <Base value=\"T\" count=\"38199106\"/>\n" +
            "          <Base value=\"N\" count=\"371506\"/>\n" +
            "        </Bases>\n" +
            "      </RUN>\n" +
            "    </RUN_SET>\n" +
            "  </EXPERIMENT_PACKAGE>\n" +
            "  <EXPERIMENT_PACKAGE>\n" +
            "    <EXPERIMENT accession=\"SRX7615553\" alias=\"HKU-SZ-005b\">\n" +
            "      <IDENTIFIERS>\n" +
            "        <PRIMARY_ID>SRX7615553</PRIMARY_ID>\n" +
            "      </IDENTIFIERS>\n" +
            "      <TITLE>Sequencing of Betacoronavirus</TITLE>\n" +
            "      <STUDY_REF accession=\"SRP242169\">\n" +
            "        <IDENTIFIERS>\n" +
            "          <PRIMARY_ID>SRP242169</PRIMARY_ID>\n" +
            "          <EXTERNAL_ID namespace=\"BioProject\">PRJNA601630</EXTERNAL_ID>\n" +
            "        </IDENTIFIERS>\n" +
            "      </STUDY_REF>\n" +
            "      <DESIGN>\n" +
            "        <DESIGN_DESCRIPTION>SISPA</DESIGN_DESCRIPTION>\n" +
            "        <SAMPLE_DESCRIPTOR accession=\"SRS6048919\">\n" +
            "          <IDENTIFIERS>\n" +
            "            <PRIMARY_ID>SRS6048919</PRIMARY_ID>\n" +
            "            <EXTERNAL_ID namespace=\"BioSample\">SAMN13898864</EXTERNAL_ID>\n" +
            "          </IDENTIFIERS>\n" +
            "        </SAMPLE_DESCRIPTOR>\n" +
            "        <LIBRARY_DESCRIPTOR>\n" +
            "          <LIBRARY_NAME>HKU-SZ-005b</LIBRARY_NAME>\n" +
            "          <LIBRARY_STRATEGY>RNA-Seq</LIBRARY_STRATEGY>\n" +
            "          <LIBRARY_SOURCE>GENOMIC</LIBRARY_SOURCE>\n" +
            "          <LIBRARY_SELECTION>RANDOM</LIBRARY_SELECTION>\n" +
            "          <LIBRARY_LAYOUT>\n" +
            "            <SINGLE/>\n" +
            "          </LIBRARY_LAYOUT>\n" +
            "        </LIBRARY_DESCRIPTOR>\n" +
            "      </DESIGN>\n" +
            "      <PLATFORM>\n" +
            "        <OXFORD_NANOPORE>\n" +
            "          <INSTRUMENT_MODEL>MinION</INSTRUMENT_MODEL>\n" +
            "        </OXFORD_NANOPORE>\n" +
            "      </PLATFORM>\n" +
            "    </EXPERIMENT>\n" +
            "    <SUBMISSION lab_name=\"NA\" center_name=\"HKU-Shenzhen Hospital\" accession=\"SRA1029819\" alias=\"SUB6865067\">\n" +
            "      <IDENTIFIERS>\n" +
            "        <PRIMARY_ID>SRA1029819</PRIMARY_ID>\n" +
            "      </IDENTIFIERS>\n" +
            "    </SUBMISSION>\n" +
            "    <Organization type=\"institute\">\n" +
            "      <Name>HKU-Shenzhen Hospital</Name>\n" +
            "      <Address postal_code=\"518053\">\n" +
            "        <Department>NA</Department>\n" +
            "        <Institution>HKU-Shenzhen Hospital</Institution>\n" +
            "        <Street>1, Haiyuan 1st Road, Futian District</Street>\n" +
            "        <City>Shenzhen</City>\n" +
            "        <Country>China</Country>\n" +
            "      </Address>\n" +
            "      <Contact email=\"jdip1007@connect.hku.hk\">\n" +
            "        <Address postal_code=\"518053\">\n" +
            "          <Department>NA</Department>\n" +
            "          <Institution>HKU-Shenzhen Hospital</Institution>\n" +
            "          <Street>1, Haiyuan 1st Road, Futian District</Street>\n" +
            "          <City>Shenzhen</City>\n" +
            "          <Country>China</Country>\n" +
            "        </Address>\n" +
            "        <Name>\n" +
            "          <First>Jonathan</First>\n" +
            "          <Last>Ip</Last>\n" +
            "          <Middle>Daniel</Middle>\n" +
            "        </Name>\n" +
            "      </Contact>\n" +
            "    </Organization>\n" +
            "    <STUDY center_name=\"BioProject\" alias=\"PRJNA601630\" accession=\"SRP242169\">\n" +
            "      <IDENTIFIERS>\n" +
            "        <PRIMARY_ID>SRP242169</PRIMARY_ID>\n" +
            "        <EXTERNAL_ID namespace=\"BioProject\" label=\"primary\">PRJNA601630</EXTERNAL_ID>\n" +
            "      </IDENTIFIERS>\n" +
            "      <DESCRIPTOR>\n" +
            "        <STUDY_TITLE>Severe acute respiratory syndrome coronavirus 2 Genome sequencing</STUDY_TITLE>\n" +
            "        <STUDY_TYPE existing_study_type=\"Whole Genome Sequencing\"/>\n" +
            "        <STUDY_ABSTRACT>Whole genome sequencing from a patient's specimen</STUDY_ABSTRACT>\n" +
            "        <CENTER_PROJECT_NAME>Wuhan seafood market pneumonia virus</CENTER_PROJECT_NAME>\n" +
            "      </DESCRIPTOR>\n" +
            "    </STUDY>\n" +
            "    <SAMPLE alias=\"HKU-SZ-005b\" accession=\"SRS6048919\">\n" +
            "      <IDENTIFIERS>\n" +
            "        <PRIMARY_ID>SRS6048919</PRIMARY_ID>\n" +
            "        <EXTERNAL_ID namespace=\"BioSample\">SAMN13898864</EXTERNAL_ID>\n" +
            "      </IDENTIFIERS>\n" +
            "      <TITLE>Microbe sample from Betacoronavirus</TITLE>\n" +
            "      <SAMPLE_NAME>\n" +
            "        <TAXON_ID>2697049</TAXON_ID>\n" +
            "        <SCIENTIFIC_NAME>Severe acute respiratory syndrome coronavirus 2</SCIENTIFIC_NAME>\n" +
            "      </SAMPLE_NAME>\n" +
            "      <SAMPLE_LINKS>\n" +
            "        <SAMPLE_LINK>\n" +
            "          <XREF_LINK>\n" +
            "            <DB>bioproject</DB>\n" +
            "            <ID>601630</ID>\n" +
            "            <LABEL>PRJNA601630</LABEL>\n" +
            "          </XREF_LINK>\n" +
            "        </SAMPLE_LINK>\n" +
            "      </SAMPLE_LINKS>\n" +
            "      <SAMPLE_ATTRIBUTES>\n" +
            "        <SAMPLE_ATTRIBUTE>\n" +
            "          <TAG>isolate</TAG>\n" +
            "          <VALUE>Sputum</VALUE>\n" +
            "        </SAMPLE_ATTRIBUTE>\n" +
            "        <SAMPLE_ATTRIBUTE>\n" +
            "          <TAG>host</TAG>\n" +
            "          <VALUE>Homo sapiens</VALUE>\n" +
            "        </SAMPLE_ATTRIBUTE>\n" +
            "        <SAMPLE_ATTRIBUTE>\n" +
            "          <TAG>collection_date</TAG>\n" +
            "          <VALUE>2020-01</VALUE>\n" +
            "        </SAMPLE_ATTRIBUTE>\n" +
            "        <SAMPLE_ATTRIBUTE>\n" +
            "          <TAG>geo_loc_name</TAG>\n" +
            "          <VALUE>China</VALUE>\n" +
            "        </SAMPLE_ATTRIBUTE>\n" +
            "        <SAMPLE_ATTRIBUTE>\n" +
            "          <TAG>sample_type</TAG>\n" +
            "          <VALUE>Sputum</VALUE>\n" +
            "        </SAMPLE_ATTRIBUTE>\n" +
            "        <SAMPLE_ATTRIBUTE>\n" +
            "          <TAG>BioSampleModel</TAG>\n" +
            "          <VALUE>Microbe, viral or environmental</VALUE>\n" +
            "        </SAMPLE_ATTRIBUTE>\n" +
            "      </SAMPLE_ATTRIBUTES>\n" +
            "    </SAMPLE>\n" +
            "    <Pool>\n" +
            "      <Member member_name=\"\" accession=\"SRS6048919\" sample_name=\"HKU-SZ-005b\" sample_title=\"Microbe sample from Betacoronavirus\" spots=\"505484\" bases=\"284575255\" tax_id=\"2697049\" organism=\"Severe acute respiratory syndrome coronavirus 2\">\n" +
            "        <IDENTIFIERS>\n" +
            "          <PRIMARY_ID>SRS6048919</PRIMARY_ID>\n" +
            "          <EXTERNAL_ID namespace=\"BioSample\">SAMN13898864</EXTERNAL_ID>\n" +
            "        </IDENTIFIERS>\n" +
            "      </Member>\n" +
            "    </Pool>\n" +
            "    <RUN_SET>\n" +
            "      <RUN accession=\"SRR10948474\" alias=\"HKU-SZ-005b.fastq\" total_spots=\"505484\" total_bases=\"284575255\" size=\"262201551\" load_done=\"true\" published=\"2020-01-26 12:12:47\" is_public=\"true\" cluster_name=\"public\" static_data_available=\"1\" filtered_data_available=\"true\">\n" +
            "        <IDENTIFIERS>\n" +
            "          <PRIMARY_ID>SRR10948474</PRIMARY_ID>\n" +
            "        </IDENTIFIERS>\n" +
            "        <EXPERIMENT_REF accession=\"SRX7615553\">\n" +
            "          <IDENTIFIERS/>\n" +
            "        </EXPERIMENT_REF>\n" +
            "        <Pool>\n" +
            "          <Member member_name=\"\" accession=\"SRS6048919\" sample_name=\"HKU-SZ-005b\" sample_title=\"Microbe sample from Betacoronavirus\" spots=\"505484\" bases=\"284575255\" tax_id=\"2697049\" organism=\"Severe acute respiratory syndrome coronavirus 2\">\n" +
            "            <IDENTIFIERS>\n" +
            "              <PRIMARY_ID>SRS6048919</PRIMARY_ID>\n" +
            "              <EXTERNAL_ID namespace=\"BioSample\">SAMN13898864</EXTERNAL_ID>\n" +
            "            </IDENTIFIERS>\n" +
            "          </Member>\n" +
            "        </Pool>\n" +
            "        <SRAFiles>\n" +
            "          <SRAFile cluster=\"public\" filename=\"SRR10948474\" url=\"https://sra-downloadb.be-md.ncbi.nlm.nih.gov/sos3/sra-pub-run-19/SRR10948474/SRR10948474.2\" size=\"262203338\" date=\"2020-01-28 22:02:47\" md5=\"49a7987cf89ec4b8be385f5d213f6515\" semantic_name=\"run\" supertype=\"Primary ETL\" sratoolkit=\"1\">\n" +
            "            <Alternatives url=\"https://sra-pub-sars-cov2.s3.amazonaws.com/run/SRR10948474/SRR10948474\" free_egress=\"worldwide\" access_type=\"anonymous\" org=\"AWS\"/>\n" +
            "            <Alternatives url=\"https://sra-downloadb.be-md.ncbi.nlm.nih.gov/sos3/sra-pub-run-19/SRR10948474/SRR10948474.2\" free_egress=\"worldwide\" access_type=\"anonymous\" org=\"NCBI\"/>\n" +
            "            <Alternatives url=\"https://storage.googleapis.com/nih-sequence-read-archive/run/SRR10948474/SRR10948474\" free_egress=\"worldwide\" access_type=\"anonymous\" org=\"GCP\"/>\n" +
            "          </SRAFile>\n" +
            "        </SRAFiles>\n" +
            "        <CloudFiles>\n" +
            "          <CloudFile filetype=\"run\" provider=\"gs\" location=\"gs.US\"/>\n" +
            "          <CloudFile filetype=\"run\" provider=\"s3\" location=\"s3.us-east-1\"/>\n" +
            "        </CloudFiles>\n" +
            "        <Statistics nreads=\"1\" nspots=\"505484\">\n" +
            "          <Read index=\"0\" count=\"505484\" average=\"562.98\" stdev=\"395.51\"/>\n" +
            "        </Statistics>\n" +
            "        <Databases>\n" +
            "          <Database>\n" +
            "            <Table name=\"SEQUENCE\">\n" +
            "              <Statistics source=\"meta\">\n" +
            "                <Rows count=\"505484\"/>\n" +
            "                <Elements count=\"284575255\"/>\n" +
            "              </Statistics>\n" +
            "            </Table>\n" +
            "          </Database>\n" +
            "        </Databases>\n" +
            "        <Bases cs_native=\"false\" count=\"284575255\">\n" +
            "          <Base value=\"A\" count=\"77825724\"/>\n" +
            "          <Base value=\"C\" count=\"62355533\"/>\n" +
            "          <Base value=\"G\" count=\"62825339\"/>\n" +
            "          <Base value=\"T\" count=\"81210233\"/>\n" +
            "          <Base value=\"N\" count=\"358426\"/>\n" +
            "        </Bases>\n" +
            "      </RUN>\n" +
            "    </RUN_SET>\n" +
            "  </EXPERIMENT_PACKAGE>\n" +
            "  <EXPERIMENT_PACKAGE>\n" +
            "    <EXPERIMENT accession=\"SRX7570458\" alias=\"HKU-SZ-002a\">\n" +
            "      <IDENTIFIERS>\n" +
            "        <PRIMARY_ID>SRX7570458</PRIMARY_ID>\n" +
            "      </IDENTIFIERS>\n" +
            "      <TITLE>Human:NPS</TITLE>\n" +
            "      <STUDY_REF accession=\"SRP242169\">\n" +
            "        <IDENTIFIERS>\n" +
            "          <PRIMARY_ID>SRP242169</PRIMARY_ID>\n" +
            "          <EXTERNAL_ID namespace=\"BioProject\">PRJNA601630</EXTERNAL_ID>\n" +
            "        </IDENTIFIERS>\n" +
            "      </STUDY_REF>\n" +
            "      <DESIGN>\n" +
            "        <DESIGN_DESCRIPTION>Whole genome sequencing</DESIGN_DESCRIPTION>\n" +
            "        <SAMPLE_DESCRIPTOR accession=\"SRS6014638\">\n" +
            "          <IDENTIFIERS>\n" +
            "            <PRIMARY_ID>SRS6014638</PRIMARY_ID>\n" +
            "            <EXTERNAL_ID namespace=\"BioSample\">SAMN13871323</EXTERNAL_ID>\n" +
            "          </IDENTIFIERS>\n" +
            "        </SAMPLE_DESCRIPTOR>\n" +
            "        <LIBRARY_DESCRIPTOR>\n" +
            "          <LIBRARY_NAME>HKU-SZ-002a</LIBRARY_NAME>\n" +
            "          <LIBRARY_STRATEGY>RNA-Seq</LIBRARY_STRATEGY>\n" +
            "          <LIBRARY_SOURCE>METAGENOMIC</LIBRARY_SOURCE>\n" +
            "          <LIBRARY_SELECTION>RANDOM</LIBRARY_SELECTION>\n" +
            "          <LIBRARY_LAYOUT>\n" +
            "            <SINGLE/>\n" +
            "          </LIBRARY_LAYOUT>\n" +
            "        </LIBRARY_DESCRIPTOR>\n" +
            "      </DESIGN>\n" +
            "      <PLATFORM>\n" +
            "        <OXFORD_NANOPORE>\n" +
            "          <INSTRUMENT_MODEL>MinION</INSTRUMENT_MODEL>\n" +
            "        </OXFORD_NANOPORE>\n" +
            "      </PLATFORM>\n" +
            "    </EXPERIMENT>\n" +
            "    <SUBMISSION lab_name=\"Microbiology\" center_name=\"University of Hong Kong\" accession=\"SRA1027117\" alias=\"SUB6821589\">\n" +
            "      <IDENTIFIERS>\n" +
            "        <PRIMARY_ID>SRA1027117</PRIMARY_ID>\n" +
            "      </IDENTIFIERS>\n" +
            "    </SUBMISSION>\n" +
            "    <Organization type=\"institute\">\n" +
            "      <Name>University of Hong Kong</Name>\n" +
            "      <Address postal_code=\"000\">\n" +
            "        <Department>Microbiology</Department>\n" +
            "        <Institution>University of Hong Kong</Institution>\n" +
            "        <Street>102 Pok Fu Lam Road, Pok Fu Lam</Street>\n" +
            "        <City>Hong Kong</City>\n" +
            "        <Country>Hong Kong</Country>\n" +
            "      </Address>\n" +
            "      <Contact email=\"jdip1007@connect.hku.hk\">\n" +
            "        <Address postal_code=\"000\">\n" +
            "          <Department>Microbiology</Department>\n" +
            "          <Institution>University of Hong Kong</Institution>\n" +
            "          <Street>102 Pok Fu Lam Road, Pok Fu Lam</Street>\n" +
            "          <City>Hong Kong</City>\n" +
            "          <Country>Hong Kong</Country>\n" +
            "        </Address>\n" +
            "        <Name>\n" +
            "          <First>Jonathan</First>\n" +
            "          <Last>Ip</Last>\n" +
            "          <Middle>Daniel</Middle>\n" +
            "        </Name>\n" +
            "      </Contact>\n" +
            "    </Organization>\n" +
            "    <STUDY center_name=\"BioProject\" alias=\"PRJNA601630\" accession=\"SRP242169\">\n" +
            "      <IDENTIFIERS>\n" +
            "        <PRIMARY_ID>SRP242169</PRIMARY_ID>\n" +
            "        <EXTERNAL_ID namespace=\"BioProject\" label=\"primary\">PRJNA601630</EXTERNAL_ID>\n" +
            "      </IDENTIFIERS>\n" +
            "      <DESCRIPTOR>\n" +
            "        <STUDY_TITLE>Severe acute respiratory syndrome coronavirus 2 Genome sequencing</STUDY_TITLE>\n" +
            "        <STUDY_TYPE existing_study_type=\"Whole Genome Sequencing\"/>\n" +
            "        <STUDY_ABSTRACT>Whole genome sequencing from a patient's specimen</STUDY_ABSTRACT>\n" +
            "        <CENTER_PROJECT_NAME>Wuhan seafood market pneumonia virus</CENTER_PROJECT_NAME>\n" +
            "      </DESCRIPTOR>\n" +
            "    </STUDY>\n" +
            "    <SAMPLE alias=\"HKU-SZ-002a\" accession=\"SRS6014638\">\n" +
            "      <IDENTIFIERS>\n" +
            "        <PRIMARY_ID>SRS6014638</PRIMARY_ID>\n" +
            "        <EXTERNAL_ID namespace=\"BioSample\">SAMN13871323</EXTERNAL_ID>\n" +
            "      </IDENTIFIERS>\n" +
            "      <TITLE>Microbe sample from Coronavirus</TITLE>\n" +
            "      <SAMPLE_NAME>\n" +
            "        <TAXON_ID>2697049</TAXON_ID>\n" +
            "        <SCIENTIFIC_NAME>Severe acute respiratory syndrome coronavirus 2</SCIENTIFIC_NAME>\n" +
            "      </SAMPLE_NAME>\n" +
            "      <SAMPLE_LINKS>\n" +
            "        <SAMPLE_LINK>\n" +
            "          <XREF_LINK>\n" +
            "            <DB>bioproject</DB>\n" +
            "            <ID>601630</ID>\n" +
            "            <LABEL>PRJNA601630</LABEL>\n" +
            "          </XREF_LINK>\n" +
            "        </SAMPLE_LINK>\n" +
            "      </SAMPLE_LINKS>\n" +
            "      <SAMPLE_ATTRIBUTES>\n" +
            "        <SAMPLE_ATTRIBUTE>\n" +
            "          <TAG>strain</TAG>\n" +
            "          <VALUE>Betacoronavirus</VALUE>\n" +
            "        </SAMPLE_ATTRIBUTE>\n" +
            "        <SAMPLE_ATTRIBUTE>\n" +
            "          <TAG>collection_date</TAG>\n" +
            "          <VALUE>2020-01</VALUE>\n" +
            "        </SAMPLE_ATTRIBUTE>\n" +
            "        <SAMPLE_ATTRIBUTE>\n" +
            "          <TAG>geo_loc_name</TAG>\n" +
            "          <VALUE>China</VALUE>\n" +
            "        </SAMPLE_ATTRIBUTE>\n" +
            "        <SAMPLE_ATTRIBUTE>\n" +
            "          <TAG>host</TAG>\n" +
            "          <VALUE>Homo sapiens</VALUE>\n" +
            "        </SAMPLE_ATTRIBUTE>\n" +
            "        <SAMPLE_ATTRIBUTE>\n" +
            "          <TAG>sample_type</TAG>\n" +
            "          <VALUE>Nasopharyngeal Swab</VALUE>\n" +
            "        </SAMPLE_ATTRIBUTE>\n" +
            "        <SAMPLE_ATTRIBUTE>\n" +
            "          <TAG>BioSampleModel</TAG>\n" +
            "          <VALUE>Microbe, viral or environmental</VALUE>\n" +
            "        </SAMPLE_ATTRIBUTE>\n" +
            "      </SAMPLE_ATTRIBUTES>\n" +
            "    </SAMPLE>\n" +
            "    <Pool>\n" +
            "      <Member member_name=\"\" accession=\"SRS6014638\" sample_name=\"HKU-SZ-002a\" sample_title=\"Microbe sample from Coronavirus\" spots=\"261890\" bases=\"90569410\" tax_id=\"2697049\" organism=\"Severe acute respiratory syndrome coronavirus 2\">\n" +
            "        <IDENTIFIERS>\n" +
            "          <PRIMARY_ID>SRS6014638</PRIMARY_ID>\n" +
            "          <EXTERNAL_ID namespace=\"BioSample\">SAMN13871323</EXTERNAL_ID>\n" +
            "        </IDENTIFIERS>\n" +
            "      </Member>\n" +
            "    </Pool>\n" +
            "    <RUN_SET>\n" +
            "      <RUN accession=\"SRR10902284\" alias=\"HKU-SZ-002a.fastq\" total_spots=\"261890\" total_bases=\"90569410\" size=\"82541246\" load_done=\"true\" published=\"2020-01-26 11:20:14\" is_public=\"true\" cluster_name=\"public\" static_data_available=\"1\" filtered_data_available=\"true\">\n" +
            "        <IDENTIFIERS>\n" +
            "          <PRIMARY_ID>SRR10902284</PRIMARY_ID>\n" +
            "        </IDENTIFIERS>\n" +
            "        <EXPERIMENT_REF accession=\"SRX7570458\">\n" +
            "          <IDENTIFIERS/>\n" +
            "        </EXPERIMENT_REF>\n" +
            "        <Pool>\n" +
            "          <Member member_name=\"\" accession=\"SRS6014638\" sample_name=\"HKU-SZ-002a\" sample_title=\"Microbe sample from Coronavirus\" spots=\"261890\" bases=\"90569410\" tax_id=\"2697049\" organism=\"Severe acute respiratory syndrome coronavirus 2\">\n" +
            "            <IDENTIFIERS>\n" +
            "              <PRIMARY_ID>SRS6014638</PRIMARY_ID>\n" +
            "              <EXTERNAL_ID namespace=\"BioSample\">SAMN13871323</EXTERNAL_ID>\n" +
            "            </IDENTIFIERS>\n" +
            "          </Member>\n" +
            "        </Pool>\n" +
            "        <SRAFiles>\n" +
            "          <SRAFile cluster=\"public\" filename=\"SRR10902284\" url=\"https://sra-downloadb.be-md.ncbi.nlm.nih.gov/sos3/sra-pub-run-21/SRR10902284/SRR10902284.2\" size=\"82543031\" date=\"2020-01-28 22:01:39\" md5=\"67992d0e956586b36203b420e792c624\" semantic_name=\"run\" supertype=\"Primary ETL\" sratoolkit=\"1\">\n" +
            "            <Alternatives url=\"https://sra-pub-sars-cov2.s3.amazonaws.com/run/SRR10902284/SRR10902284\" free_egress=\"worldwide\" access_type=\"anonymous\" org=\"AWS\"/>\n" +
            "            <Alternatives url=\"https://sra-downloadb.be-md.ncbi.nlm.nih.gov/sos3/sra-pub-run-21/SRR10902284/SRR10902284.2\" free_egress=\"worldwide\" access_type=\"anonymous\" org=\"NCBI\"/>\n" +
            "            <Alternatives url=\"https://storage.googleapis.com/nih-sequence-read-archive/run/SRR10902284/SRR10902284\" free_egress=\"worldwide\" access_type=\"anonymous\" org=\"GCP\"/>\n" +
            "          </SRAFile>\n" +
            "        </SRAFiles>\n" +
            "        <CloudFiles>\n" +
            "          <CloudFile filetype=\"run\" provider=\"gs\" location=\"gs.US\"/>\n" +
            "          <CloudFile filetype=\"run\" provider=\"s3\" location=\"s3.us-east-1\"/>\n" +
            "        </CloudFiles>\n" +
            "        <Statistics nreads=\"1\" nspots=\"261890\">\n" +
            "          <Read index=\"0\" count=\"261890\" average=\"345.83\" stdev=\"116.35\"/>\n" +
            "        </Statistics>\n" +
            "        <Databases>\n" +
            "          <Database>\n" +
            "            <Table name=\"SEQUENCE\">\n" +
            "              <Statistics source=\"meta\">\n" +
            "                <Rows count=\"261890\"/>\n" +
            "                <Elements count=\"90569410\"/>\n" +
            "              </Statistics>\n" +
            "            </Table>\n" +
            "          </Database>\n" +
            "        </Databases>\n" +
            "        <Bases cs_native=\"false\" count=\"90569410\">\n" +
            "          <Base value=\"A\" count=\"21742728\"/>\n" +
            "          <Base value=\"C\" count=\"22436039\"/>\n" +
            "          <Base value=\"G\" count=\"22393344\"/>\n" +
            "          <Base value=\"T\" count=\"23731759\"/>\n" +
            "          <Base value=\"N\" count=\"265540\"/>\n" +
            "        </Bases>\n" +
            "      </RUN>\n" +
            "    </RUN_SET>\n" +
            "  </EXPERIMENT_PACKAGE>\n" +
            "</EXPERIMENT_PACKAGE_SET>";

    private final String _validXml2 = "<?xml version=\"1.0\" ?>\n" +
            "<EXPERIMENT_PACKAGE_SET>\n" +
            "<EXPERIMENT_PACKAGE><EXPERIMENT accession=\"ERX9342043\" alias=\"Exp_Run652_BC27\" broker_name=\"ERASMUS MC, UNIVERISTY MEDICAL CENTER\" center_name=\"Dutch COVID-19 response team\"><IDENTIFIERS><PRIMARY_ID>ERX9342043</PRIMARY_ID></IDENTIFIERS><TITLE>GridION sequencing; GridION sequencing</TITLE><STUDY_REF accession=\"ERP122482\"><IDENTIFIERS><PRIMARY_ID>ERP122482</PRIMARY_ID><EXTERNAL_ID namespace=\"BioProject\">PRJEB39014</EXTERNAL_ID></IDENTIFIERS></STUDY_REF><DESIGN><DESIGN_DESCRIPTION/><SAMPLE_DESCRIPTOR accession=\"ERS12118950\"><IDENTIFIERS><PRIMARY_ID>ERS12118950</PRIMARY_ID><EXTERNAL_ID namespace=\"BioSample\">SAMEA14491513</EXTERNAL_ID></IDENTIFIERS></SAMPLE_DESCRIPTOR><LIBRARY_DESCRIPTOR><LIBRARY_NAME/><LIBRARY_STRATEGY>AMPLICON</LIBRARY_STRATEGY><LIBRARY_SOURCE>VIRAL RNA</LIBRARY_SOURCE><LIBRARY_SELECTION>PCR</LIBRARY_SELECTION><LIBRARY_LAYOUT><SINGLE/></LIBRARY_LAYOUT></LIBRARY_DESCRIPTOR></DESIGN><PLATFORM><OXFORD_NANOPORE><INSTRUMENT_MODEL>GridION</INSTRUMENT_MODEL></OXFORD_NANOPORE></PLATFORM><EXPERIMENT_ATTRIBUTES><EXPERIMENT_ATTRIBUTE><TAG>library preparation date</TAG><VALUE>not collected</VALUE></EXPERIMENT_ATTRIBUTE></EXPERIMENT_ATTRIBUTES></EXPERIMENT><SUBMISSION accession=\"ERA14833035\" alias=\"SUBMISSION-30-05-2022-17:57:03:671\" broker_name=\"Submission account for department of Viroscienceâ€™s Research Teams, ErasmusMC\" center_name=\"ERASMUS MC, UNIVERISTY MEDICAL CENTER\" lab_name=\"European Nucleotide Archive\"><IDENTIFIERS><PRIMARY_ID>ERA14833035</PRIMARY_ID></IDENTIFIERS><TITLE>Submitted by ERASMUS MC, UNIVERISTY MEDICAL CENTER on 30-MAY-2022</TITLE></SUBMISSION><Organization type=\"center\"><Name abbr=\"ERASMUS MC, UNIVERISTY MEDICAL CENTER\">ERASMUS MC, UNIVERISTY MEDICAL CENTER</Name></Organization><STUDY accession=\"ERP122482\" alias=\"ena-STUDY-ERASMUS MC, UNIVERISTY MEDICAL CENTER-23-06-2020-11:10:53:348-690\" center_name=\"ERASMUS MC, UNIVERISTY MEDICAL CENTER\"><IDENTIFIERS><PRIMARY_ID>ERP122482</PRIMARY_ID><EXTERNAL_ID namespace=\"BioProject\">PRJEB39014</EXTERNAL_ID></IDENTIFIERS><DESCRIPTOR><STUDY_TITLE>Continued SARS-CoV-2 whole-genome sequencing for outbreak tracking and tracing in the Netherlands</STUDY_TITLE><STUDY_TYPE existing_study_type=\"Other\"/><STUDY_ABSTRACT>SARS-CoV-2 is a novel coronavirus that has rapidly spread across the globe. In the Netherlands, the first case of SARS-CoV-2 has been notified on the 27th of February. Here we describe the continued whole-genome sequencing effort in the Netherlands used for outbreak tracking and tracing.</STUDY_ABSTRACT><CENTER_PROJECT_NAME>Continued SARS-CoV-2 WGS Netherlands</CENTER_PROJECT_NAME><STUDY_DESCRIPTION>SARS-CoV-2 is a novel coronavirus that has rapidly spread across the globe. In the Netherlands, the first case of SARS-CoV-2 has been notified on the 27th of February. Here we describe the continued whole-genome sequencing effort in the Netherlands used for outbreak tracking and tracing.</STUDY_DESCRIPTION></DESCRIPTOR><STUDY_ATTRIBUTES><STUDY_ATTRIBUTE><TAG>ENA-FIRST-PUBLIC</TAG><VALUE>2021-01-05</VALUE></STUDY_ATTRIBUTE><STUDY_ATTRIBUTE><TAG>ENA-LAST-UPDATE</TAG><VALUE>2020-06-23</VALUE></STUDY_ATTRIBUTE></STUDY_ATTRIBUTES></STUDY><SAMPLE accession=\"ERS12118950\" alias=\"Run652_BC27\" broker_name=\"Submission account for department of Viroscienceâ€™s Research Teams, ErasmusMC\" center_name=\"Dutch COVID-19 response team\"><IDENTIFIERS><PRIMARY_ID>ERS12118950</PRIMARY_ID><EXTERNAL_ID namespace=\"BioSample\">SAMEA14491513</EXTERNAL_ID></IDENTIFIERS><TITLE>Dutch COVID-19 response sample sequencing</TITLE><SAMPLE_NAME><TAXON_ID>2697049</TAXON_ID><SCIENTIFIC_NAME>Severe acute respiratory syndrome coronavirus 2</SCIENTIFIC_NAME><COMMON_NAME>Human coronavirus 2019</COMMON_NAME></SAMPLE_NAME><DESCRIPTION>A SARS-CoV-2 specfic multiplex PCR for Nanopore sequencing was performed, similar to amplicon-based approaches as previously described. In short, primers for 86 overlapping amplicons spanning the entire genome were designed using primal. The amplicon length was set to 500bp with 75bp overlap between the different amplicons. The libraries were generated using the native barcode kits from Nanopore (EXP-NBD104 and EXP-NBD114 and SQK-LSK109) and sequenced on a R9.4 flow cell multiplexing up to 24 samples per sequence run. Raw data was demultiplexed, amplicon primers were trimmed and human data was removed by mapping against the human reference genome.</DESCRIPTION><SAMPLE_ATTRIBUTES><SAMPLE_ATTRIBUTE><TAG>collecting institution</TAG><VALUE>not provided</VALUE></SAMPLE_ATTRIBUTE><SAMPLE_ATTRIBUTE><TAG>collection date</TAG><VALUE>2022-05-02</VALUE></SAMPLE_ATTRIBUTE><SAMPLE_ATTRIBUTE><TAG>collector name</TAG><VALUE>Dutch COVID-19 response team</VALUE></SAMPLE_ATTRIBUTE><SAMPLE_ATTRIBUTE><TAG>geographic location (country and/or sea)</TAG><VALUE>Netherlands</VALUE></SAMPLE_ATTRIBUTE><SAMPLE_ATTRIBUTE><TAG>geographic location (region and locality)</TAG><VALUE>Zuid-Holland</VALUE></SAMPLE_ATTRIBUTE><SAMPLE_ATTRIBUTE><TAG>GISAID Accession ID</TAG><VALUE>EPI_ISL_12685215</VALUE></SAMPLE_ATTRIBUTE><SAMPLE_ATTRIBUTE><TAG>host common name</TAG><VALUE>Human</VALUE></SAMPLE_ATTRIBUTE><SAMPLE_ATTRIBUTE><TAG>host health state</TAG><VALUE>not collected</VALUE></SAMPLE_ATTRIBUTE><SAMPLE_ATTRIBUTE><TAG>host scientific name</TAG><VALUE>Homo sapiens</VALUE></SAMPLE_ATTRIBUTE><SAMPLE_ATTRIBUTE><TAG>host sex</TAG><VALUE>not provided</VALUE></SAMPLE_ATTRIBUTE><SAMPLE_ATTRIBUTE><TAG>host subject id</TAG><VALUE>restricted access</VALUE></SAMPLE_ATTRIBUTE><SAMPLE_ATTRIBUTE><TAG>isolate</TAG><VALUE>hCoV-19/Netherlands/ZH-EMC-5880/2022</VALUE></SAMPLE_ATTRIBUTE><SAMPLE_ATTRIBUTE><TAG>isolation source host-associated</TAG><VALUE>not collected</VALUE></SAMPLE_ATTRIBUTE><SAMPLE_ATTRIBUTE><TAG>sample capture status</TAG><VALUE>active surveillance in response to outbreak</VALUE></SAMPLE_ATTRIBUTE><SAMPLE_ATTRIBUTE><TAG>ENA-FIRST-PUBLIC</TAG><VALUE>2022-05-30</VALUE></SAMPLE_ATTRIBUTE><SAMPLE_ATTRIBUTE><TAG>ENA-LAST-UPDATE</TAG><VALUE>2022-05-30</VALUE></SAMPLE_ATTRIBUTE></SAMPLE_ATTRIBUTES></SAMPLE><RUN_SET><RUN accession=\"ERR9794591\" alias=\"Run652_BC27\" broker_name=\"Submission account for department of Viroscienceâ€™s Research Teams, ErasmusMC\" center_name=\"Dutch COVID-19 response team\" load_done=\"false\" unavailable=\"true\" published=\"2022-05-31 06:11:37\" is_public=\"true\"><IDENTIFIERS><PRIMARY_ID>ERR9794591</PRIMARY_ID></IDENTIFIERS><TITLE>GridION sequencing; GridION sequencing</TITLE><EXPERIMENT_REF accession=\"ERX9342043\"><IDENTIFIERS><PRIMARY_ID>ERX9342043</PRIMARY_ID></IDENTIFIERS></EXPERIMENT_REF><RUN_ATTRIBUTES><RUN_ATTRIBUTE><TAG>ENA-FIRST-PUBLIC</TAG><VALUE>2022-05-30</VALUE></RUN_ATTRIBUTE><RUN_ATTRIBUTE><TAG>ENA-LAST-UPDATE</TAG><VALUE>2022-05-30</VALUE></RUN_ATTRIBUTE></RUN_ATTRIBUTES></RUN></RUN_SET></EXPERIMENT_PACKAGE></EXPERIMENT_PACKAGE_SET>\n";
}
