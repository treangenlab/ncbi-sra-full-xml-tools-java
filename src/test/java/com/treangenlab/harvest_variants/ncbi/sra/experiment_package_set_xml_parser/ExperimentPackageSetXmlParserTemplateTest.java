package com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

public class ExperimentPackageSetXmlParserTemplateTest
{
    @Test
    public void testDoesStreamAppearToBeStartingNewExperimentPackage_Empty() throws IOException
    {
        PushbackInputStreamSize2 emptyStream = new PushbackInputStreamSize2(new InputStream()
        {
            @Override
            public int read()
            {
                return -1;
            }
        });


        boolean isStarting =
                ExperimentPackageSetXmlParserTemplate.doesStreamAppearToBeStartingNewExperimentPackage(emptyStream);

        Assert.assertFalse(isStarting);
    }

}
