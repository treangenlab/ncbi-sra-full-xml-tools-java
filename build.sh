#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

mvn -f $SCRIPT_DIR/pom.xml clean package dependency:copy-dependencies


